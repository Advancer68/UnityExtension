﻿using System;

namespace CxExtension
{
	public static class UTimer
	{
		private static UnityTimer instance { get { return UnityTimer.instance; } }

		/// <summary>
		/// 添加一个延迟回调
		/// </summary>
		/// <param name="delay"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		public static TimeOne AddTimer(float delay, Action func)
		{
			instance.enabled = true;
			var t = instance.TimerMng.AddTimer(delay, func);
			return t;
		}

		/// <summary>
		/// 添加一个重复的延迟回调
		/// </summary>
		/// <param name="delay"></param>
		/// <param name="repeatMax"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		public static TimeRepeat AddTimerRepeat(float delay, int repeatMax, Action<int> func)
		{
			instance.enabled = true;
			var t = instance.TimerMng.AddTimerRepeat(delay,repeatMax,func);
			return t;
		}

		public static TimeRepeat AddTimerRepeat(float delay, int repeatMax, Action<int> func, Action comlete)
		{
			var t = instance.TimerMng.AddTimerRepeat(delay, repeatMax, func,comlete);
			return t;
		}

		public static void StopTimer(ITimer timer)
		{
			instance.TimerMng.Stop(timer);
		}
		/// <summary>
		/// 添加一个重复永久的延迟回调
		/// </summary>
		/// <param name="delay"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		public static TimeRepeat AddTimerRepeat(float delay, Action<int> func)
		{
			return AddTimerRepeat(delay, int.MaxValue, func);
		}

	}

	public static class UTimerExtension
	{
	
	}
}