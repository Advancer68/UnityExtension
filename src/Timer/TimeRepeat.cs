﻿using System;

namespace CxExtension
{
	public class TimeRepeat: CTimer
	{
		private int mCurCount = 0;                      //	
		private int mCount;                      //
		public event Action<int> onRepeat;//

		public TimeRepeat(float interval,int count) : base(interval)
		{
			mCount = count;
		}

		public void SetCount(int v)
		{
			mCount = v;
		}


		public int Count { get { return mCount; } }


		private void RepeatOneInternal()
		{
			if (onRepeat != null) onRepeat(mCurCount);
		}

		public override void Stop()
		{
			base.Stop();
			mCurCount = mCount;
		}

		public override bool Update(float dt)
		{
			if (base.Update(dt))
			{
				RepeatOneInternal();
				mCurCount++;
				if (mCurCount >= mCount)
				{
					ComleteInternal();
					return true;
				}
			}

			return false;
		}
	}
}