﻿namespace CxExtension
{
	public interface ITimer
	{
		bool Update(float dt);
		void Stop();
	}
}