﻿using System;

namespace CxExtension
{
	/// <summary>
	/// 时间满足后 回调一个函数
	/// </summary>
	public class CTimerV : ITimer
	{
		private Action mAction;
		private ITimer mTimer;                      //
		public CTimerV(ITimer timer,Action func)
		{
			this.mAction = func;
			mTimer = timer;
		}
		public void SetFunc(Action func)
		{
			this.mAction = func;
		}
		public bool Update(float dt)
		{
			var result = mTimer.Update(dt);
			if (result)
			{
				mAction();
			}
			return result;
		}

		public void Stop()
		{
			mTimer.Stop();
		}
	}
}