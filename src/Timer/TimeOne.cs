﻿namespace CxExtension
{
	public class TimeOne : CTimer
	{
		public TimeOne(float duration) : base(duration)
		{

		}
		public override bool Update(float dt)
		{
			if (base.Update(dt))
			{
				if (State == TState.Complete)
				{
					ComleteInternal();
				}
				return true;
			}
			return false;
		}
	}
}