﻿using UnityEngine;

namespace CxExtension
{
	public class UnityTimer : SingletonMono<UnityTimer>
	{
		private readonly CTimerMng mTimerMng = new CTimerMng();                      //

		public CTimerMng TimerMng { get { return mTimerMng; } }

		//public static void Add(float)

	
		private void FixedUpdate()
		{

			if (mTimerMng.Update(Time.fixedDeltaTime))
			{
				enabled = false;
			}
		}
	}
}