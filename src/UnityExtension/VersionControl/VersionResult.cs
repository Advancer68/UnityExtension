﻿using System;
using System.Collections.Generic;

namespace VersionControl
{
	public class VersionResult<T>
	{
		public List<T> AddList = new List<T>();
		public List<T> ModifiedList = new List<T>();
		public List<T> DeleteList = new List<T>();
	}
}