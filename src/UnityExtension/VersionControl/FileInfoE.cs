﻿using System;
using System.Collections.Generic;
using System.IO;
using CxExtension;

namespace VersionControl
{
	public class FileInfoE:IComparable<FileInfoE>,IComparer<FileInfoE>
	{
		public string relativePath;
		public long lastWriteTime;
		public long length;
		public string md5;

		public static FileInfoE CreateByPath(string relativePath,string root)
		{
			var path = PathUtil.Combine(root,relativePath);
			var fileInfo = new FileInfo(path);
			var lastWriteTime = fileInfo.LastWriteTimeUtc;

			var info = new FileInfoE()
			{
				relativePath = relativePath,
				lastWriteTime = lastWriteTime.Ticks,
				length = fileInfo.Length,
				md5 = fileInfo.GetHashStr(),
			};
			return info;
		}

		public static int CompareS(FileInfoE a, FileInfoE b)
		{
			var timeResult = a.lastWriteTime > b.lastWriteTime ? 1 : -1;;
			if (a.length != b.length)
			{
				return timeResult;
			}

			var md5Result = string.Compare(a.md5,b.md5,StringComparison.OrdinalIgnoreCase);
			if (md5Result == 0)
			{
				return 0;
			}
			else
			{
				return timeResult;
			}
		}
		#region Implementation of IComparable<AssetBundleInfo>

		public int CompareTo(FileInfoE other)
		{
			return CompareS(this, other);
		}

		#endregion

		#region Implementation of IComparer<AssetBundleInfo>

		public int Compare(FileInfoE x, FileInfoE y)
		{
			return CompareS(x, y);
		}

		#endregion
	}
}