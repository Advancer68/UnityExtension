﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VersionControl
{
	[Serializable]
	public class AssetBundleVariantInfo
	{
		public string Name;//
		public List<string> Variant = new List<string>();//
		
	}
	[Serializable]
	public class AssetBundleInfo:IComparable<AssetBundleInfo>,IComparer<AssetBundleInfo>
	{
		public string Name;//
		public string RelativePath;//
		public string Variant;//
		public long Length;
		public string LastWriteTime;
		public long LastWriteTimeSecond;
		public string Md5;
		public StoreRootPathId storeRootPathId = StoreRootPathId.StreamingPath;

		/// <summary>
		/// 比较大小,如果a大返回值大于零,等于返回零.a小返回小于零
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static int CompareS(AssetBundleInfo a, AssetBundleInfo b)
		{
			var aNull = a == null;
			var bNull = b == null;
			if (aNull && bNull)
			{
				return 0;
			}
			if (aNull || bNull)
			{
				return bNull? 1 : -1;
			}
			var mdcom = string.Compare(a.Md5, b.Md5, StringComparison.OrdinalIgnoreCase);
			if (mdcom == 0)
			{
				return 0;
			}
			else
			{
				long result = a.LastWriteTimeSecond - b.LastWriteTimeSecond;
				if (result == 0)
				{
					//AssetBundleInfo的md5不一样，时间一定不一样
					//return 0;
				}
				return result > 0 ? 1 : -1;
			}
			
		}

		public static AssetBundleInfo Max(params AssetBundleInfo[] argns)
		{
			if (argns.Length == 0) return null;

			if (argns.Length ==1)
			{
				return argns[0];
			}

			return argns.Max();
		}
		public bool ValidFile(string rootPath)
		{
			var path = PathUtil.Combine(rootPath,Name);
			if (File.Exists(path) == false)
			{
				return false;
			}
			var fileInfo = new FileInfo(path);
			if (fileInfo.Length != Length)
			{
				return false;
			}

			return true;
		}


		#region Implementation of IComparable<AssetBundleInfo>

		public int CompareTo(AssetBundleInfo other)
		{
			return CompareS(this,other);
		}

		#endregion

		#region Implementation of IComparer<AssetBundleInfo>

		int IComparer<AssetBundleInfo>.Compare(AssetBundleInfo x, AssetBundleInfo y)
		{
			return CompareS(x, y);
		}

		#endregion
	}
}