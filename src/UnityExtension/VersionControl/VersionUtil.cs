﻿using CxExtension;

namespace VersionControl
{
	public class VersionUtil
	{

		public static VersionResult<FileInfoE> CompareDirectory(string src,string dest)
		{
			var resultS = DirectoryUtil.CompareDirectory(src,dest,null);
			var result = new VersionResult<FileInfoE>();
			foreach (var it in resultS.leftOnly)
			{
				var info = FileInfoE.CreateByPath(it,src);
				result.DeleteList.Add(info);
			}
			foreach (var it in resultS.bothHave)
			{
				var infoLeft = FileInfoE.CreateByPath(it, src);
				var infoRight = FileInfoE.CreateByPath(it, dest);
				var compare = infoLeft.CompareTo(infoRight);
				if (compare < 0)
				{
					result.ModifiedList.Add(infoRight);
				}
			}
			foreach (var it in resultS.rightOnly)
			{
				var info = FileInfoE.CreateByPath(it, dest);
				result.AddList.Add(info);
			}
			return result;
		}

	}
}