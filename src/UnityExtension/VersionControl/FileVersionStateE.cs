﻿namespace VersionControl
{
	public enum FileVersionStateE
	{
		New,
		Modified,
		Delete,
	}
}