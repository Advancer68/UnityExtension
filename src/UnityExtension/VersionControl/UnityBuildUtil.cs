﻿using UnityEditor;

namespace VersionControl
{
	public static class UnityBuildUtil
	{
		public static string GenerateVersionString(string appVersion, int buildCode)
		{
			var versionStr = appVersion.Replace('.', '_');
			var str = $"{versionStr}_{buildCode}";
			return str;
		}

	}
}