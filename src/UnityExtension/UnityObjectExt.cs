﻿namespace UnityEngine
{
	//using Object = UnityEngine.Object;
	public static class UnityObjectExt
	{
		/// <summary>
		/// if unity object or default value any one has content then return true,otherwise return false
		/// </summary>
		/// <param name="self"></param>
		/// <param name="defaultValue"></param>
		/// <param name="resultValue"></param>
		/// <returns></returns>
		public static bool HasValueOrDefaultU<T>(this T self, T defaultValue, out T resultValue) where T:Object
		{
			var result = self.IsNullU()?defaultValue:self;
			resultValue = result;
			return result.IsNoNullU();
		}

		/// <summary>
		/// if unity object is null then return true
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static bool IsNullU(this Object self)
		{
			return self.IsRefNull();
		}
		public static bool IsRefNull(this Object self)
		{
			var obj = (object)self;
			var value = obj == null;
			return value;
		}
		/// <summary>
		/// if unity object is not null then return true
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static bool IsNoNullU(this Object self)
		{
			return self.IsNullU() == false;
		}
	}
}