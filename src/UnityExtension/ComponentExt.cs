﻿using System;
using JetBrains.Annotations;

namespace UnityEngine
{
	public static class ComponentExt
	{
		/// <summary>
		/// foreach components list use pool list
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="action"></param>
		/// <param name="includeInactive"></param>
		public static void GetComponentsInChildrenForeach<T>([NotNull] this Component self, Action<T> action,
			bool includeInactive)
		{
			self.gameObject.GetComponentsInChildrenForeach(action,includeInactive);
		}
		/// <summary>
		/// foreach components list use pool list
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="action"></param>
		/// <param name="includeInactive"></param>
		public static void GetComponentsInParentForeach<T>([NotNull] this Component self, Action<T> action,
			bool includeInactive)
		{
			self.gameObject.GetComponentsInParentForeach(action,includeInactive);
		}
		/// <summary>
		/// foreach components list use pool list
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="action"></param>
		public static void GetComponentsForeach<T>([NotNull] this Component self, Action<T> action)
		{
			self.gameObject.GetComponentsForeach(action);
		}
	}
}