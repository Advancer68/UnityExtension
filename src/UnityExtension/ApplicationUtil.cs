﻿#define UNITY_STANDALONE_WIN

using System.IO;

namespace UnityEngine
{
	public static class ApplicationUtil
	{
		static ApplicationUtil()
		{
			var projectDir = Path.GetDirectoryName(Application.dataPath);
			projectDir = PathUtil.ToUnixPath(projectDir); //
			ProjectPath = projectDir;
			TempPath = PathUtil.Combine(projectDir, "Temp");
			LibraryPath = PathUtil.Combine(projectDir, "Library");
			StreamingAssets = PathUtil.Combine(projectDir, "StreamingAssets");
		}
		public static string ProjectPath { get; }
		public static string StreamingAssets { get; }

		public static string LibraryPath { get; }
		public static string TempPath { get; }
	}
}