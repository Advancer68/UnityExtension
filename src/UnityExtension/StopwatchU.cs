﻿namespace UnityEngine
{
	public class StopwatchU
	{
		private float mStartTime; //
		private float mStopTime; //
		private float mCostMax; //
		public float Cost { get; private set; }//
		public StopwatchU(float costMax)
		{
			mCostMax = costMax;
		}
		public void Start()
		{
			mStartTime = Time.realtimeSinceStartup;
		}
		public void Stop()
		{
			var cost = Sample();
			Cost = cost;
		}

		public float Sample()
		{
			mStopTime = Time.realtimeSinceStartup;
			var cost = mStopTime - mStartTime;
			return cost;
		}
		public bool Check()
		{
			var cost = Sample();
			if (cost >= mCostMax)
			{
				return true;
			}

			return false;
		}
	}
}