﻿using System;
using UnityEngine;

namespace UnityExtensions
{
	public class TransformPoolAsync : UObjectPool<Transform>
	{
		private Transform mParent;                      //
		private string mUiName;

		public Transform Parent { get { return mParent; } }

		public string UiName
		{
			get
			{
				return mUiName;
			}
		}

		public void SetUiName(string v)
		{
			mUiName = v;
		}

		public TransformPoolAsync(Transform parent,Action<Action<Transform>> createNew,Action<Transform> getAction,Action<Transform> recycleAction)
		{
			mParent = parent;
			CreateNewAsync += createNew;
			mGetAction = getAction;
			mResetAction = recycleAction;
		}

		public TransformPoolAsync()
		{
			mParent = SingletonTransform.PoolTransf;
		}

	}
}