﻿using UnityEngine;

namespace UnityExtensions
{
	public class TransformPool : UObjectPoolImp<Transform>

	{
		#region Overrides of UObjectPoolImp<Transform>

		protected override void OnResetOne(Transform it)
		{
			base.OnResetOne(it);
			it.SetActive(false);
			it.SetParent(mParent,false);
		}

		#endregion
	}
}