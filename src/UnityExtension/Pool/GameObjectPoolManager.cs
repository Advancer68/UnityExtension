﻿using UnityEngine;

namespace UnityExtensions
{
	public class GameObjectPoolManager : UObjectPoolManager<GameObject>
	{
		public GameObjectPoolManager()
		{
			
		}
		public GameObjectPoolManager(Transform parent) : base(parent)
		{
		}

		public GameObjectPoolManager(PoolCreateD<GameObject> poolCreate) : base(poolCreate)
		{
		}
	}
}