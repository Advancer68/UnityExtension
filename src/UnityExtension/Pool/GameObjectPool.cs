﻿using UnityEngine;

namespace UnityExtensions
{
	public class GameObjectPool : UObjectPoolImp<GameObject>
	{
		public GameObjectPool(GameObject prefab, Transform parent = null) : base(prefab, parent)
		{
			prefab.transform.SetParent(parent,false);
		}
		#region Overrides of UObjectPoolImp<Transform>

		#region Overrides of UObjectPoolImp<GameObject>

		protected override GameObject CreateNew()
		{
			var it = base.CreateNew();
			it.transform.SetParent(mParent, false);
			return it;
		}

		protected override void OnGet(GameObject it)
		{
			base.OnGet(it);
			//it.SetActive(true);
		}

		#endregion

		protected override void OnResetOne(GameObject it)
		{
			base.OnResetOne(it);
			//it.SetActive(false);
			it.transform.SetParent(mParent, false);
		}

		#endregion
	}
}