﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UnityExtensions
{
	/// <summary>
	/// default pool implement
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class UObjectPoolImp<T> : UObjectPool<T> where T : Object
	{
		protected Transform mParent;                      //
		protected T mPrefab;                      //
		
		public UObjectPoolImp(T prefab,Func<T> createNew, Action<T> resetFunc):base(createNew,resetFunc)
		{
			mPrefab = prefab;
		}

		public UObjectPoolImp()
		{
			
		}

		public UObjectPoolImp(T prefab):this(prefab,null)
		{

		}

		public UObjectPoolImp(T prefab,Transform parent = null)
		{
			mPrefab = prefab;
			mParent = parent;
			SetCreateNew(CreateNew);
			SetResetAction(OnResetOne);
			SetGetAction(OnGet);
		}

		#region Event

		protected virtual T CreateNew()
		{
			var va = Object.Instantiate(mPrefab);
			return va;
		}

		protected virtual void OnGet(T it)
		{
		}

		protected virtual void OnResetOne(T it)
		{
		}
		#endregion

		#region Overrides of UObjectPool<Transform>

		public override void Clear(Action<T> callAction)
		{
			base.Clear(callAction);
			Object.Destroy(mPrefab);
		}

		#endregion
	}
}