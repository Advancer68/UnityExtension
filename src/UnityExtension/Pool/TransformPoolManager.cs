﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace UnityExtensions
{
	public class TransformPoolManager:UObjectPoolManager<Transform>
	{
		public TransformPoolManager(Transform parent) : base(parent)
		{
		}

		public TransformPoolManager(PoolCreateD<Transform> poolCreate) : base(poolCreate)
		{
		}
	}
}