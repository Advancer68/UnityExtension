﻿using System;

namespace UnityEngine
{
	public static class ColorUtilityEx
	{
		public static void SetValueAll(this Color[] self, Color value)
		{
			if (self == null) throw new ArgumentNullException("self");
			unsafe
			{
				fixed (Color* arr = self)
				{
					var length = self.Length;
					for (var i = 0; i < length; i++)
					{
						arr[i] = value;
					}
				}
			}
		}
		public static Color ParseHtmlString(string htmlStr)
		{
			Color c;
			if (!ColorUtility.TryParseHtmlString(htmlStr, out c))
			{
				Debug.LogErrorFormat("颜色解析错误:{0},返回白色", htmlStr);
				c = Color.white;
			}

			return c;
		}
	}
}