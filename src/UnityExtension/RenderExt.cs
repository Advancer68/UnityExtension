﻿namespace UnityEngine
{
	public static class RenderExt
	{
		private static readonly MaterialPropertyBlock mCacheBlock = new MaterialPropertyBlock();//

		/// <summary>
		/// get static cache property block
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static MaterialPropertyBlock GetPropertyBlock(this Renderer self)
		{
			mCacheBlock.Clear();
			self.GetPropertyBlock(mCacheBlock);
			return mCacheBlock;
		}
		/// <summary>
		/// set static cache property block
		/// </summary>
		/// <param name="self"></param>
		public static void SetPropertyBlock(this Renderer self)
		{
			self.SetPropertyBlock(mCacheBlock);
		}
	}
}