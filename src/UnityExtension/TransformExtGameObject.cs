﻿using System;
using JetBrains.Annotations;
using UnityExtensions;

namespace UnityEngine
{
	public static class TransformExtGameObject
	{

		public static void GetChildComponent<T>(this Transform self, string childName, Action<T> action)
		{
			self.Find(childName, it =>
			{
				it.GetComponent(action);
			});
		}

		public static T GetComponentInParentAndSelf<T>(this Component self, Action<T> action)
		{
			var t = self.GetComponent<T>();
			if (t == null)
			{
				t = self.GetComponentInParent<T>();
			}

			if (t != null && action != null)
			{
				action(t);
			}
			return t;
		}

		public static void GetChildComponent<T>([NotNull] this Transform self, [NotNull] string childName, out T com)
		{
			if (self == null) throw new ArgumentNullException("self");
			if (childName == null) throw new ArgumentNullException("childName");
			var t = default(T);
			self.GetChildComponent<T>(childName, it => t = it);
			com = t;
		}


		public static void GetComponent<T>(this Transform self, Action<T> action, bool logOnNot)
		{
			if (self == null)
			{
				Debug.LogErrorFormat("the self is null");
				return;
			}
			var com = self.GetComponent<T>();
			if (com == null && logOnNot)
			{
				Debug.LogWarningFormat("{0}上面没有{1}组件", self.GetTransformPath(), typeof(T).Name); return;
			}

			if (action != null)
			{
				action(com);
			}
		}

		public static void GetComponent<T>(this Transform self, Action<T> action)
		{
			self.GetComponent<T>(action, true);
		}
		public static T AddComponent<T>(this Transform self) where T : Component
		{
			return self.gameObject.AddComponent<T>();
		}
		/// <summary>
		/// 找到某个名称的子节点 获取T类型的组件
		/// </summary>
		/// <typeparam name="T">类型</typeparam>
		/// <param name="self">自己</param>
		/// <param name="name">要找寻的节点名称</param>
		/// <returns></returns>
		public static T FindChildComponentByName<T>(this Transform self, string name) where T : Component
		{
			if (self == null)
			{
				Debug.LogErrorFormat("空引用的父节点 找子节点:{0}", name);
				return null;
			}

			if (string.IsNullOrEmpty(name))
			{
				Debug.LogErrorFormat("无效的名称:{0}", name);
				return null;
			}

			var transf = self.FindChildByName(name);
			if (transf == null)
			{
				Debug.LogErrorFormat("没有找到子节点:{0}", name);
				return null;
			}

			var cpnt = transf.GetComponent<T>();
			if (cpnt == null)
			{
				Debug.LogErrorFormat("{0}节点下面没有{1}组件", transf.name, typeof(T).Name);
			}
			return cpnt;
		}
		public static T GetComponentOrAdd<T>(this Transform trans) where T : Component, new()
		{
			return trans.gameObject.GetComponentOrAdd<T>();
		}

		/// <summary>
		/// 设置transform是否激活扩展函数
		/// </summary>
		/// <param name="trans">transform</param>
		/// <param name="active">是否激活</param>
		public static void SetActive(this Transform trans, bool active)
		{
			if (null != trans)
			{
				trans.gameObject.SetActive(active);
			}
		}

		public static void SetActive(this Transform self, string childname, bool active)
		{
			var child = self.Find(childname);
			if (child)
			{
				child.SetActive(active);
			}
		}
	}
}