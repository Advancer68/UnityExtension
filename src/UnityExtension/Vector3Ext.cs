﻿using UnityEngine;

namespace UnityExtensions
{
	public static class Vector3Ext
	{

		public static Vector3 InverseLerp(this Vector3 self,Vector3 from,Vector3 to)
		{
			var v = new Vector3
			{
				x = Mathf.InverseLerp(@from.x, to.x, self.x),
				y = Mathf.InverseLerp(@from.y, to.y, self.y),
				z = Mathf.InverseLerp(@from.z, to.z, self.z)
			};
			return v;
		}
		public static Vector3 NormalizedOne(this Vector3 self)
		{
			var max = 0.0f;
			for (var i = 0; i < 3; i++)
			{
				var it = self[i];
				if (it > max)
				{
					max = it;
				}
			}

			if (max > 0)
			{
				return new Vector3(self.x / max, self.y / max, self.z / max);
			}
			else
			{
				Debug.LogFormat("Vector:{0} max great 0", self);
				return self;
			}
		}

	}
}