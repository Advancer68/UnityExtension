﻿using UnityEngine;
using System.Collections;
namespace UnityExtensions
{
	public static class AnimatorUtility
	{
        public static void SetStateId(this Animator self,int id)
        {
            self.SetInteger("StateId", id);
        }
	}
}
