﻿using System;
using JetBrains.Annotations;

namespace UnityEngine
{
	public static class ComponentGetExt
	{

		public static ParticleSystemRenderer GetParticleSystemRenderer([NotNull] this Component self)
		{
			if (self == null) throw new ArgumentNullException(nameof(self));

			var particleSystem = self.GetComponent<ParticleSystem>();
			if (particleSystem.IsRefNull())
			{
				Debug.LogErrorFormat("{0} not find:ParticleSystem", self.transform.GetTransformPath());
				return null;
			}
			var rend = particleSystem.GetComponent<ParticleSystemRenderer>();
			return rend;
		}

	}
}