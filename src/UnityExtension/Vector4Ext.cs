﻿using UnityEngine;

namespace UnityExtensions
{
	public static class Vector4Ext
	{
		public static Bounds ToBoundsByMinMax(this Vector4 self)
		{
			var bound = new Bounds()
			{
				min = new Vector3(self.x,self.y),
				max = new Vector3(self.z,self.w),
			};
			return bound;
		}

	}
}