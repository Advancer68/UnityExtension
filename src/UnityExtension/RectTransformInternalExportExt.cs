﻿namespace UnityEngine
{
	/// <summary>
	/// 导出RectTransform Internal 实现的函数
	/// </summary>
	public static class RectTransformInternalExportExt
	{
		public static Rect GetRectInParentSpace(this RectTransform self)
		{
			Rect rect = self.rect;
			Vector2 vector = self.offsetMin + Vector2.Scale(self.pivot, rect.size);
			Transform parent = self.parent;
			if (parent != null)
			{
				RectTransform component = parent.GetComponent<RectTransform>();
				if (component != null)
				{
					vector += Vector2.Scale(self.anchorMin, component.rect.size);
				}
			}
			rect.x += vector.x;
			rect.y += vector.y;
			return rect;
		}
	}
}