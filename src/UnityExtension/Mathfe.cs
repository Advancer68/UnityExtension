﻿using UnityEngine;

namespace CxExtension
{
	public class Mathfe
	{
		public static int ClampRing0(int value, int max)
		{
			return ClampRing(value, 0, max);
		}
		public static int ClampRing(int value, int min, int max)
		{
			if (value < min)
			{
				value = max + 1 + value;
				value = Mathf.Clamp(value, min, max);
			}

			else if (value > max)
				value = max;
			return value;
		}
		/// <summary>
		/// 限制索引在某个区间,包含min,不包含max
		/// </summary>
		/// <param name="value">if value 小于 min,value = max + value</param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		/// <returns></returns>
		public static int ClampRingIndex(int value, int min, int max)
		{
			if (max < 1)
			{
				Debug.LogErrorFormat("max is the index max,can not is:{0}", max);
			}
			if (value < min)
			{
				value = max + value;
				value = Mathf.Clamp(value, min, max);
			}

			else if (value > max)
				value = max;
			return value;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <param name="max"></param>
		/// <returns></returns>
		public static int ClampRingIndex(int value, int max)
		{
			return ClampRingIndex(value, 0, max);
		}

		public static int ClampRingIndexBy(int value, int min, int offset)
		{
			return ClampRingIndex(value, min, min + offset);
		}

		public static int ClampRingIndexBy(int value, int offset)
		{
			return ClampRingIndexBy(value, 0, offset);
		}

		/// <summary>
		/// 弧长转换成对应圆的角度
		/// </summary>
		/// <param name="radius">圆的半径</param>
		/// <param name="arcLength">弧长</param>
		/// <returns></returns>
		public static float Arc2Angle(float arcLength, float radius)
		{
			var perimeter = 2 * Mathf.PI * radius;
			arcLength *= 360;
			return arcLength / perimeter;
		}

		/// <summary>
		///求某个角度的sin
		/// </summary>
		/// <param name="angle">0-360</param>
		/// <returns></returns>
		public static float Sin(float angle)
		{
			return Mathf.Sin(angle * Mathf.Deg2Rad);
		}
		/// <summary>
		/// 求某个角度的cos
		/// </summary>
		/// <param name="angle">0-360</param>
		/// <returns></returns>
		public static float Cos(float angle)
		{
			return Mathf.Cos(angle * Mathf.Deg2Rad);
		}

		public static Vector2 Angle2Pos(float angle)
		{
			var x = Mathfe.Sin(angle);
			var y = Mathfe.Cos(angle);
			return new Vector2(x, y);
		}

		public static Vector2 Angle2Pos(float angle, float radius)
		{
			return Angle2Pos(angle) * radius;
		}
	}
}