﻿using CxExtension;
using UnityEngine;


namespace UnityExtensions
{
	public static class RectExt
	{
		public static Vector4 ToVector4(this Rect self)
		{
			return new Vector4(self.xMin,self.yMin,self.xMax,self.yMax);
		}
		public static Bounds ToBounds(this Rect self)
		{
			var bound = new Bounds(self.center,self.size);
			return bound;
		}

		/// <summary>
		/// get rect 4 corner point
		/// </summary>
		/// <param name="self"></param>
		/// <param name="fourCornersArray">must size great 4</param>
		public static void GetCorners(this Rect self, Vector3[] fourCornersArray)
		{
			if (fourCornersArray == null || fourCornersArray.Length < 4)
			{
				Debug.LogError((object)"Calling GetLocalCorners with an array that is null or has less than 4 elements.");
			}
			else
			{
				Rect rect = self;
				float x = rect.x;
				float y = rect.y;
				float xMax = rect.xMax;
				float yMax = rect.yMax;
				fourCornersArray[0] = new Vector3(x, y, 0.0f);
				fourCornersArray[1] = new Vector3(x, yMax, 0.0f);
				fourCornersArray[2] = new Vector3(xMax, yMax, 0.0f);
				fourCornersArray[3] = new Vector3(xMax, y, 0.0f);
			}
		}
		public static void GetQuadVectors(this Rect self, Vector3[] vectors)
		{
			if (vectors == null || vectors.Length < 4)
			{
				Debug.LogError((object)"Calling GetLocalCorners with an array that is null or has less than 4 elements.");
			}
			else
			{
				Rect rect = self;
				float x = rect.x;
				float y = rect.y;
				float xMax = rect.xMax;
				float yMax = rect.yMax;
#if UNITY_2021_3_OR_NEWER
				/*23
				 *01
				 * 031 302
				 */
				vectors[0] = new Vector3(x, y, 0.0f);
				vectors[1] = new Vector3(xMax, y, 0.0f);
				vectors[2] = new Vector3(x, yMax, 0.0f);
				vectors[3] = new Vector3(xMax, yMax, 0.0f);
#elif UNITY_2017_1_OR_NEWER
				/*31
				 *02
				 * 012 103
				 */
				vectors[0] = new Vector3(x, y, 0.0f);
				vectors[1] = new Vector3(xMax, yMax, 0.0f);
				vectors[2] = new Vector3(xMax, y, 0.0f);
				vectors[3] = new Vector3(x, yMax, 0.0f);
#endif
			}
		}

		public static void GetWorldCorners(this Rect self,Transform transform, Vector3[] fourCornersArray)
		{
			self.GetCorners(fourCornersArray);
			transform.TransformPoint(fourCornersArray);
		}
		public static Vector2 Clamp(this Rect self, Vector2 point)
		{
			var po = point;
			po.x = Mathf.Clamp(point.x,self.xMin,self.xMax);
			po.y = Mathf.Clamp(point.y,self.yMin,self.yMax);
			return po;
		}

		public static Vector2 ClampAxis(this Rect self, Vector2 point, int axis)
		{
			return self.ClampAxis(point,(RectTransform.Axis) axis);
		}
		public static Vector2 ClampAxis(this Rect self, Vector2 point,RectTransform.Axis axis)
		{
			var po = point;
			switch (axis)
			{
				case RectTransform.Axis.Horizontal:
					po.x = Mathf.Clamp(point.x, self.xMin, self.xMax);
					break;
				case RectTransform.Axis.Vertical:
					po.y = Mathf.Clamp(point.y, self.yMin, self.yMax);
					break;
			}
			return po;
		}
		/// <summary>
		/// 偏移固定单位的数值,返回新的rect
		/// </summary>
		/// <param name="self"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="w"></param>
		/// <param name="h"></param>
		/// <returns></returns>
		public static Rect OffsetRect(this Rect self, float x, float y, float w, float h)
		{
			self.x += x;
			self.y += y;
			self.width += w;
			self.height += h;
			return self;
		}

		public static Rect OffsetRect(this Rect self, float x, float y)
		{
			return self.OffsetRect(x, y, 0, 0);
		}

		public static Rect OffsetRectY(this Rect self, float y)
		{
			return self.OffsetRect(0, y);
		}
		public static Rect OffsetRectX(this Rect self, float x)
		{
			return self.OffsetRect(x, 0);
		}
		public static Rect GetOffsetRect(this Rect self, float x, float y,float w,float h)
		{
			var newr = self;
			return newr.OffsetRect(x, y, w, h);
		}
	}
}