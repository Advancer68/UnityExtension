﻿using UnityEngine.UI;

namespace UnityEngine.EventSystems
{
	/// <summary>
	/// ui事件射线穿透脚本,允许设置是否把当前ui事件传递到下一层级的ui
	/// </summary>
	public class CanvasRaycastPiercer : UIBehaviour, ICanvasRaycastPiercer
	{
		[SerializeField]
		protected bool m_PierceRaycast = false;
		public bool PierceRaycast
		{
			get { return m_PierceRaycast; }
			set
			{
				SetPropertyUtility.SetStruct(ref m_PierceRaycast, value);
			}
		}

		public virtual bool IsRaycastPiercerValid(BaseEventData eventData)
		{
			return m_PierceRaycast;
		}
	}
}