﻿using System;

namespace UnityEngine.EventSystems
{
	/// <summary>
	/// 射线检测穿透接口,
	/// </summary>
	public interface ICanvasRaycastPiercer
	{
		/// <summary>
		///   <para>Given a point and a camera is the raycast piercer valid.</para>
		/// </summary>
		/// <param name="eventData"></param>
		/// <returns>
		///   <para>if return true the event can transmit next gameobject</para>
		/// </returns>
		bool IsRaycastPiercerValid(BaseEventData eventData);
	}
}