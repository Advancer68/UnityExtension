﻿using UnityEngine.EventSystems;
using UnityExtensions;

namespace UnityEngine.UI
{
	[AddComponentMenu("Layout/Content Position Fitter", 141)]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class ContentPositionFitter: UIBehaviour
	{
		[SerializeField]
		private FitterTypeE mFitter = 0;                      //

		private RectTransform mRectTransform;                      //

		public RectTransform rectTransform
		{
			get
			{
				if (mRectTransform == null)
				{
					mRectTransform = GetComponent<RectTransform>();
				}
				return mRectTransform;
			}
		}


		public void SetPosition(Vector2 newPos)
		{
			transform.SetUiPositionFitterInParentRect(newPos, (FitterTypeE)mFitter);
		}

		[ContextMenu("GetRect")]
		public void TestGetRect()
		{
			Debug.LogFormat("rect is:{0}",rectTransform.rect);
			Debug.LogFormat("rect is:{0}",rectTransform.GetRectInParentSpace());
		}
	}
}
