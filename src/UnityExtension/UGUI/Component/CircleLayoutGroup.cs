﻿using CxExtension;

namespace UnityEngine.UI
{

	public enum CircleLayoutUnitE
	{
		Pixel,//基于像素
		Angle,//基于角度

	}

	[AddComponentMenu("Layout/Circle Layout Group", 152)]
	public class CircleLayoutGroup : LayoutGroup
	{
		public CircleLayoutUnitE LayoutUnit = CircleLayoutUnitE.Pixel;
		[SerializeField]
		[Range(0f,1f)]
		protected float mCircleAlignment = 0;//圆形的半径
		public float CircleAlignment { get { return mCircleAlignment; } set { SetProperty(ref mCircleAlignment, value); } }
		[SerializeField]
		protected float mStartAngle = 0;//单元格的圆形长度
		public float StartAngle { get { return mStartAngle; } set { SetProperty(ref mStartAngle, value); } }

		[SerializeField]
		protected float mRadius = 10;//圆形的半径
		public float Radius { get { return mRadius; } set { SetProperty(ref mRadius, value); } }
		[SerializeField]
		protected float mCellSize = 10;//单元格的圆形长度
		public float CellSize { get { return mCellSize; } set { SetProperty(ref mCellSize, value); } }
		[SerializeField]
		protected float m_Spacing = 0;//间隔弧长
		public float spacing { get { return m_Spacing; } set { SetProperty(ref m_Spacing, value); } }
		public override void CalculateLayoutInputVertical()
		{
			//throw new NotImplementedException();

		}

		public override void SetLayoutHorizontal()
		{
			//throw new NotImplementedException();
		}

		public void SetLayoutBySlice(int num)
		{
			LayoutUnit = CircleLayoutUnitE.Angle;
			CellSize = 360f / num;
			spacing = 0;
		}
		public override void SetLayoutVertical()
		{
			var cellAngle = CellSize;
			var spaceAngle = spacing;
			if (LayoutUnit == CircleLayoutUnitE.Pixel)
			{
				cellAngle = Mathfe.Arc2Angle(CellSize, Radius);
				spaceAngle = Mathfe.Arc2Angle(spacing, Radius);
			}
			var totalAngle = cellAngle * rectChildren.Count + spaceAngle * (rectChildren.Count - 1) - cellAngle;
			var offsetAngle = totalAngle * CircleAlignment;
			for (var i = 0; i < rectChildren.Count; i++)
			{
				var it = rectChildren[i];
				var angle = StartAngle + cellAngle * i + spaceAngle * i - offsetAngle;
				var pos = Mathfe.Angle2Pos(angle, Radius);
#if false
				
				Debug.LogFormat("{2} angle:{0},pos:{1},offset:-{3},totalangle:{4},cellAngle:{5},spaceAngle:{6}",
					angle, pos, it.name,offsetAngle,totalAngle,cellAngle,spaceAngle);
	
#endif
				it.anchoredPosition = pos;
			}
			//SetChildAlongAxis();
			//throw new NotImplementedException();
		}
	}
}
