﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	public class ButtonPress : MonoBehaviour
		, IPointerDownHandler
		, IPointerUpHandler
	{
		[SerializeField]
		private float mInterval = 0.2f;                      //间隔时间
		[SerializeField]
		private float mDelay = 0.2f;                      //延迟时间
		private float mNextPressTime;                      //
		private bool mPressed;                      //是否已经按下
		private Button mButton;                      //
													 // Use this for initialization
		void Start()
		{
			mButton = GetComponent<Button>();
			mButton.onClick.AddListener(OnClick);
		}

		// Update is called once per frame
		void LateUpdate()
		{
			if(Time.realtimeSinceStartup > mNextPressTime && mPressed && mButton.isActiveAndEnabled)
			{
				mButton.onClick.Invoke();
			}
		}
		

		public void OnPointerDown(PointerEventData eventData)
		{
			mPressed = true;
			mNextPressTime = Time.realtimeSinceStartup + mDelay;
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			mPressed = false;
		}


		private void OnClick()	
		{
			mNextPressTime = Time.realtimeSinceStartup + mInterval;
		}

	}

}
