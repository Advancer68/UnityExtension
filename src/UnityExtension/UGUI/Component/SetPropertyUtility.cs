﻿// Decompiled with JetBrains decompiler
// Type: UnityEngine.UI.SetPropertyUtility
// Assembly: UnityEngine.UI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: FE35C8E8-4ADC-4570-9159-4A7CFC6ED9E1
// Assembly location: D:\ProgramFiles\Unity201746f1\Editor\Data\UnityExtensions\Unity\GUISystem\UnityEngine.UI.dll

using System.Collections.Generic;

namespace UnityEngine.UI
{
	internal static class SetPropertyUtility
	{
		public static bool SetColor(ref Color currentValue, Color newValue)
		{
			if ((double)currentValue.r == (double)newValue.r && (double)currentValue.g == (double)newValue.g && ((double)currentValue.b == (double)newValue.b && (double)currentValue.a == (double)newValue.a))
				return false;
			currentValue = newValue;
			return true;
		}

		public static bool SetStruct<T>(ref T currentValue, T newValue) where T : struct
		{
			if (EqualityComparer<T>.Default.Equals(currentValue, newValue))
				return false;
			currentValue = newValue;
			return true;
		}

		public static bool SetClass<T>(ref T currentValue, T newValue) where T : class
		{
			if ((object)currentValue == null && (object)newValue == null || (object)currentValue != null && currentValue.Equals((object)newValue))
				return false;
			currentValue = newValue;
			return true;
		}
	}
}
