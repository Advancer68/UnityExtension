﻿using UnityEngine;
using UnityEngine.UI;

namespace UnityExtensions
{
	public static class UnityUIExt
	{
		/// <summary>
		/// null检车后 安全设置text文本
		/// </summary>
		/// <param name="self"></param>
		/// <param name="text"></param>
		public static void SetTextSafe(this Text self, string text)
		{
			if(self == null)
				return;
			self.text = text;
		}

		public static void SetSpriteSafe(this Image self, Sprite sprite)
		{
			if(self == null)
				return;
			self.sprite = sprite;
		}
		public static void SetToggleGroupSafe(this Toggle self, ToggleGroup group)
		{
			if(self == null)
				return;
			self.group = group;
		}
	}
}