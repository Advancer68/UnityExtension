using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UnityEngine.UI
{
	public static class LayoutUtilityExt
	{
		public static float GetMinSize(RectTransform rect, int axis, ILayoutElement ignoreLayoutElement)
		{
			if (axis == 0)
				return GetMinWidth(rect, ignoreLayoutElement);
			return GetMinHeight(rect, ignoreLayoutElement);
		}

		public static float GetPreferredSize(RectTransform rect, int axis, ILayoutElement ignoreLayoutElement)
		{
			if (axis == 0)
				return GetPreferredWidth(rect, ignoreLayoutElement);
			return GetPreferredHeight(rect, ignoreLayoutElement);
		}

		public static float GetFlexibleSize(RectTransform rect, int axis, ILayoutElement ignoreLayoutElement)
		{
			if (axis == 0)
				return GetFlexibleWidth(rect,ignoreLayoutElement);
			return GetFlexibleHeight(rect, ignoreLayoutElement);
		}

		public static float GetMinWidth(RectTransform rect, ILayoutElement ignoreLayoutElement)
		{
			return GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.minWidth, 0);
		}

		public static float GetPreferredWidth(RectTransform rect, ILayoutElement ignoreLayoutElement)
		{
			return Mathf.Max(GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.minWidth, 0),
				GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.preferredWidth, 0));
		}

		public static float GetFlexibleWidth(RectTransform rect, ILayoutElement ignoreLayoutElement)
		{
			return GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.flexibleWidth, 0);
		}

		public static float GetMinHeight(RectTransform rect, ILayoutElement ignoreLayoutElement)
		{
			return GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.minHeight, 0);
		}

		public static float GetPreferredHeight(RectTransform rect, ILayoutElement ignoreLayoutElement)
		{
			return Mathf.Max(GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.minHeight, 0),
				GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.preferredHeight, 0));
		}

		public static float GetFlexibleHeight(RectTransform rect, ILayoutElement ignoreLayoutElement)
		{
			return GetLayoutProperty(rect, e => ignoreLayoutElement == e ? 0 : e.flexibleHeight, 0);
		}

		public static float GetLayoutProperty(RectTransform rect, System.Func<ILayoutElement, float> property,
			float defaultValue)
		{
			ILayoutElement dummy;
			return LayoutUtility.GetLayoutProperty(rect, property, defaultValue, out dummy);
		}
	}
}
