﻿using UnityExtensions;

namespace UnityEngine
{
	public static class GizmosE
	{
		private static readonly Vector3[] sCorners = new Vector3[4];//

		public static void DrawRectTransformBound(this RectTransform self)
		{
			self.GetWorldCorners(sCorners);
			DrawRect(sCorners);
		}

		public static void DrawTransformBounds(this Transform self, Bounds bounds)
		{
			self.GetWorldCorners(bounds,sCorners);
			DrawRect(sCorners);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="self"></param>
		/// <param name="rect">base transform corners rect</param>
		public static void DrawTransformRect(this Transform self,Rect rect)
		{
			self.GetWorldCorners(rect,sCorners);
			DrawRect(sCorners);
		}

		public static void DrawRect(Vector3[] path)
		{
			var color = Gizmos.color;
			Gizmos.color = Color.green;
			GizmosE.DrawPoly(path);
			Gizmos.color = color;
		}
		public static void DrawPoly(Vector3[] path)
		{
			for (int i = 0, length = path.Length; i < length; i++)
			{
				var it = path[i];
				var it1 = path[(i+1)%length];
				Gizmos.DrawLine(it,it1);
			}
		}
	}
}