﻿using UnityEngine;

namespace UnityExtensions
{
	public static class BoundsExt
	{

		public static void TransformPointByMatrix3x4(this Bounds self, Matrix4x4 transf)
		{
			self.min = transf.MultiplyPoint3x4(self.min);
			self.max = transf.MultiplyPoint3x4(self.max);
		}
		public static void TransformPointByMatrix(this Bounds self, Matrix4x4 transf)
		{
			self.min = transf.MultiplyPoint(self.min);
			self.max = transf.MultiplyPoint(self.max);
		}

		public static Vector3 GetSizeNormalized(this Bounds self)
		{
			var size = self.size;
			return size.NormalizedOne();
		}
		public static Rect ToRect(this Bounds self)
		{
			return new Rect { min = self.min, max = self.max };
		}
		public static void GetQuadVectors(this Bounds self, Vector3[] vectors)
		{
			var rect = ToRect(self);
			rect.GetQuadVectors(vectors);
		}
	}
}