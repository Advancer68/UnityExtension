﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace CxExtension
{
	public class ObjectPoolManager
	{
		protected Dictionary<Type, IPoolBase> mPoolMap = new Dictionary<Type, IPoolBase>();

		public T Get<T>() where T : new()
		{
			var pool = mPoolMap.GetOrNewAddF(typeof(T),it=> new ObjectPool<T>()) as ObjectPool<T>;
			return pool.Get();
		}

		public void Recycle<T>([NotNull] T value) where T : new()
		{
			if (value == null) throw new ArgumentNullException("value");

			var poolBase = mPoolMap.GetOrNewAddF(value.GetType(), it => new ObjectPool<T>());
			var pool = poolBase as ObjectPool<T>;
			pool.Recycle(value);
		}
	}
}