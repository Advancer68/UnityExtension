﻿public class NumUtil
{
	public static string ToNString(long value)
	{
		return string.Format("{0:N0}", value);
	}
}