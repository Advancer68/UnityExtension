﻿using System;

namespace CxExtension
{
	public static class StringEnumExt
	{

		public static TEnum ToEnum<TEnum>(this string self) where TEnum : struct
		{
			if (Enum.TryParse<TEnum>(self, out var result) == false)
			{
				;
			}

			return result;
		}

	}
}