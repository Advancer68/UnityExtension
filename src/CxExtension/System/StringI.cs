﻿namespace CxExtension
{
	/// <summary>
	/// string的优化版本函数,i 代表improve改良的
	/// </summary>
	public static class StringI
	{

		public static bool EqualsOrdinal(string a,string b)
		{
			var value = string.CompareOrdinal(a,b);
			return value == 0;
		}

	}
}