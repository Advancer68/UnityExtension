﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CxExtension
{
	public class AssemblyCache:CacheDictionary<string,Type>
	{
		public AssemblyCache(Assembly assembly)
		{
			FlushCache(assembly);
		}

		public void FlushCache(Assembly assembly)
		{
			CacheMap.Clear();
			try
			{
				var types = assembly.GetTypes();
				types.ToDictionary(it => it.FullName, CacheMap);
			}
			catch (ReflectionTypeLoadException  ex)
			{
				foreach (var it in ex.LoaderExceptions)
				{
					
				}
			}
		}
	}
}