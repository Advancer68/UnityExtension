﻿using System;

namespace CxExtension
{
	public static class DateTimeExt
	{
		private const long TicksPerMillisecond = 10000;
		private const long TicksPerSecond = 10000000;
		private const long TicksPerMinute = 600000000;
		private const long TicksPerHour = 36000000000;
		private const long TicksPerDay = 864000000000;
		private const int MillisPerSecond = 1000;
		private const int MillisPerMinute = 60000;
		private const int MillisPerHour = 3600000;
		private const int MillisPerDay = 86400000;
		private const int DaysPerYear = 365;
		private const int DaysPer4Years = 1461;
		private const int DaysPer100Years = 36524;
		private const int DaysPer400Years = 146097;
		private const int DaysTo1601 = 584388;
		private const int DaysTo1899 = 693593;
		private const int DaysTo10000 = 3652059;

		public static long TotalMillisecondL(this DateTime self)
		{
			var ms = self.Ticks / TicksPerMillisecond;
			return ms;
		}

		public static double TotalMillisecondD(this DateTime self)
		{
			var ms = self.Ticks / (double)TicksPerMillisecond;
			return ms;
		}
		public static long TotalSecondL(this DateTime self)
		{
			var ms = self.Ticks / TicksPerSecond;
			return ms;
		}
		public static double TotalSecondD(this DateTime self)
		{
			var ms = self.Ticks / (double)TicksPerSecond;
			return ms;
		}

	}
}