﻿using System;

namespace CxExtension
{
	public static class EnumExt
	{
	
		public static bool HasFlag(this Enum self,Enum value)
		{
			var selfInt = self.GetIntValue();
			var valueInt = value.GetIntValue();
			return selfInt.HasFlag(valueInt);
		}
		public static Enum RemoveFlag(this Enum self, Enum value)
		{
			var selfInt = self.GetIntValue();
			var valueInt = value.GetIntValue();
			var result = selfInt.RemoveFlag(valueInt);
			var newEnum = (Enum)Enum.ToObject(self.GetType(),result);
			return newEnum;
		}

	}
}