﻿using System;
using System.Collections.Generic;
namespace CxExtension.Hashset
{
	public static class HashSetExtention
	{
		public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source) => new HashSet<T>(source);
		public static void CompareDiff<T>(this HashSet<T> self,HashSet<T> destHashSet,HashSet<T> leftOnly,HashSet<T> bothHave,HashSet<T> rightOnly)
		{
			rightOnly.UnionWith(destHashSet);
			foreach (var it in self)
			{
				if (rightOnly.Remove(it))//right is have then both have
				{
					bothHave.Add(it);
				}
				else//not have in right then left only have
				{
					leftOnly.Add(it);
				}
			}
		}

		public static T Find<T>(this HashSet<T> self,Predicate<T> func)
		{
			T t = default(T);
			foreach(var item in self)
			{
				if(func(item))
				{
					t = item;
					break;
				}
			}
			return t;
		}
	}
}
