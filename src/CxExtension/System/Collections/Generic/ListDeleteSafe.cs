﻿/*************************************************
  Copyright (C), 2014-2017, lmd Tech. Co., Ltd.
  文 件  名:	Src\CxExtension\CList.cs
  作       者:	陈佳
  版       本:	1.0
  完成日期:	2017//
  功能描述:	可以在调用自身提供的遍历函数中安全删除添加元素
  主要功能:
*************************************************/

using System;
using System.Collections.Generic;
using CxExtension;

namespace CxExtension
{
	/// <summary>
	/// 可以在遍历中安全删除元素的可变数组
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ListDeleteSafe<T>: CollectionDeleteSafeTemplate<List<T>,T>
	{

#if false
		private readonly List<T> m_list = new List<T>();
		private readonly Queue<T> m_addQue = new Queue<T>();
		private readonly Queue<T> m_removeQue = new Queue<T>();
		private bool m_foreaching;

		public int Count
		{
			get { return m_list.Count + m_addQue.Count - m_removeQue.Count; }
		}

		/// <summary>
		/// 添加元素,可以在ForechAll函数遍历中使用
		/// </summary>
		/// <param name="it"></param>
		public void Add(T it)
		{
			if(it == null)
			{
				Debuger.ErrFormat("it参数不能为null");
				return;
			}
			if(m_foreaching)
			{
				addAddQue(it);
			}
			else
			{
				add_(it);
			}
		}

		public void Clear()
		{
			m_list.Clear();
			m_addQue.Clear();
			m_removeQue.Clear();
		}

		public bool Contains(T it)
		{
			if(it == null)
			{
				Debuger.ErrFormat("it参数不能为null");
				return false;
			}
			var contain = m_list.Contains(it) || m_addQue.Contains(it);
			return contain;
		}

		/// <summary>
		/// 遍历所有元素,可以在遍历中安全的删除元素
		/// </summary>
		/// <param name="func"></param>
		public void ForechAll(Action<T> func)
		{
			if(func == null)
			{
				Debuger.ErrFormat("遍历函数不能为空");
				return;
			}
			if(m_list.Count > 0)
			{
				m_foreaching = true;
				for(var i = 0;i < m_list.Count;i++)
				{
					var it = m_list[i];
					func(it);
				}
				m_foreaching = false;
			}
			//移除遍历中要删除的元素,和添加遍历中要添加的元素
			m_removeQue.DequeueAll(remove_);
			m_addQue.DequeueAll(add_);
		}

		/// <summary>
		/// 移除某个元素,可以在调用ForechAll函数遍历中使用,
		/// </summary>
		/// <param name="it"></param>
		public void Remove(T it)
		{
			if(it == null)
			{
				Debuger.ErrFormat("it参数不能为null");
				return;
			}
			if(m_foreaching)
			{
				addRemoveQue(it);
			}
			else
			{
				remove_(it);
			}
		}

		/// <summary>
		/// 直接添加某个元素
		/// </summary>
		/// <param name="it"></param>
		private void add_(T it)
		{
			m_list.Add(it);
		}

		/// <summary>
		/// 添加到待添加队列
		/// </summary>
		/// <param name="it"></param>
		private void addAddQue(T it)
		{
			if(m_addQue.Contains(it))
			{
				return;
			}
			m_addQue.Enqueue(it);
		}

		/// <summary>
		/// 添加到移除队列
		/// </summary>
		/// <param name="it"></param>
		private void addRemoveQue(T it)
		{
			if(m_removeQue.Contains(it))
			{
				return;
			}
			m_removeQue.Enqueue(it);
		}

		/// <summary>
		/// 直接移除某个元素
		/// </summary>
		/// <param name="it"></param>
		private void remove_(T it)
		{
			m_list.Remove(it);
		}
	
#endif
		public T this[int index]
		{
			get { return mCollection[index]; }
			set { mCollection[index] = value; }
		}
		#region Overrides of CollectionDeleteSafeTemplate<List<T>,T>


		protected override void foreach_(Action<T> func)
		{
			for (var i = 0; i < mCollection.Count; i++)
			{
				var it = mCollection[i];
				func(it);
			}
		}

		protected override void foreach_(Func<T, bool> func,Action<T> selectAction)
		{
			for (var i = 0; i < mCollection.Count; i++)
			{
				var it = mCollection[i];
				if (func(it) == false) continue;
				if (selectAction != null) selectAction(it);
			}
		}

		#endregion
	}
}