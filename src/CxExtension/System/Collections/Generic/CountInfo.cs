﻿using UnityEngine;

namespace CxExtension
{
	public class CountInfo<K>
	{
		public K Key { get; internal set; }//
		public virtual int Count { get; internal set; }//
		public float StartTime { get; internal set; }
		public int StartFrame { get; internal set; }

		#region Overrides of Object

		public float CostTime => Time.realtimeSinceStartup - StartTime;
		public int CostFrame => Time.frameCount - StartFrame;
		public override string ToString()
		{
			return string.Format("{0}:{1}", Key, Count);
		}

		public void Init()
		{
			Count = 0;
			StartTime = Time.realtimeSinceStartup;
			StartFrame = Time.frameCount;
		}
		#endregion
	}
}