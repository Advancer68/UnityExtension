﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace CxExtension
{
	public static class ICollectionExtension
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="list"></param>
		/// <param name="actionFunc"></param>
		public static void AddBy<T>(this ICollection<T> self, ICollection<T> list, Func<T, T> actionFunc)
		{
			foreach (var it in list)
			{
				var item = actionFunc == null ? it : actionFunc(it);
				self.Add(item);
			}
		}
		/// <summary>
		/// 如果has == true 添加list的所有元素到self,然后清理list 使 has = false
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="list"></param>
		/// <param name="has"></param>
		public static void Add<T>(this ICollection<T> self, ICollection<T> list, ref bool has)
		{
			if (list != null && has)
			{
				foreach (var item in list)
				{
					self.Add(item);
				}
				list.Clear();
				has = false;
			}
		}
		/// <summary>
		/// 如果has == true 移除list的所有元素从self中,然后清理list, 使 has = false
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="list"></param>
		/// <param name="has"></param>
		public static void Remove<T>(this ICollection<T> self, ICollection<T> list, ref bool has)
		{
			if (list != null && has)
			{
				foreach (var item in list)
				{
					self.Remove(item);
				}
				list.Clear();
				has = false;
			}
		}
	}
}
