﻿/*************************************************
  Copyright (C), 2014-2017, lmd Tech. Co., Ltd.
  文 件  名:		Assets\Scripts\Tools\CxExtension\QueueExt.cs
  作       者:	陈佳
  版       本:	1.0
  完成日期:		2017/08/15
  功能描述:		Queue的扩展函数类
  主要功能:		1,出队所有元素;
					
*************************************************/

using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace CxExtension
{
	public static class QueueExt
	{


		/// <summary>
		/// Dequeue所有的元素
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="func"></param>
		public static void DequeueAll<T>([NotNull] this Queue<T> self, [NotNull] Action<T> func)
		{
			if (self == null) throw new ArgumentNullException("self");
			if (func == null) throw new ArgumentNullException("func");
			while(self.Count > 0)
			{
				var it = self.Dequeue();
				func(it);
			}
		}
		
		/// <summary>
		/// Dequeue所有的元素,if return true break dequeue
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="func">if return true break dequeue</param>
		public static void DequeueAllF<T>([NotNull] this Queue<T> self, [NotNull] Func<T,bool> func)
		{
			if (self == null) throw new ArgumentNullException("self");
			if (func == null) throw new ArgumentNullException("func");
			while(self.Count > 0)
			{
				var it = self.Dequeue();
				if (func(it))
				{
					break;
				}
			}
		}
	}
}