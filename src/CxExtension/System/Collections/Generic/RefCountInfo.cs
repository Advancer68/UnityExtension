﻿using System.Collections.Generic;
using System.Text;

namespace CxExtension
{
	public class RefCountInfo<K,V> : CountInfo<K>
	{
		private static readonly StringBuilder build = new StringBuilder(100);
		private readonly HashSet<V> mRefList = new HashSet<V>(); //

		public bool AddRef(V value)
		{
			return mRefList.Add(value);
		}

		public bool RemoveRef(V value)
		{
			return mRefList.Remove(value);
		}

		#region Overrides of CountInfo<K>

		public override int Count
		{
			get => mRefList.Count;
			internal set => base.Count = value;
		}

		public override string ToString()
		{
			build.Clear();
			build.AppendLine(Key.ToString());
			build.AppendLineFormat("ref {0} by:",mRefList.Count);
			foreach (var it in mRefList)
			{
				build.AppendFormatLine("\t{0}",it);
			}
			return build.ToString();
		}

		#endregion
	}
}