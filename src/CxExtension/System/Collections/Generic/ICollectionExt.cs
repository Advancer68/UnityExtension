﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CxExtension
{
	public static class ICollectionExt
	{

		/// <summary>
		/// if index in(0,len) then return true
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="idx"></param>
		/// <returns></returns>
		public static bool CheckIndex(this ICollection self, int idx)
		{
			return idx > 0 && idx < self.Count;
		}
		/// <summary>
		/// return self.Count > 0
		/// </summary>
		/// <param name="self"></param>
		/// <returns>if count > 0 return true</returns>
		public static bool HasItem(this ICollection self)
		{
			return self.Count > 0;
		}
		public static bool IsNullList(this ICollection self)
		{
			return self.Count == 0;
		}
		public static bool IsEmpty(this ICollection self)
		{
			return self.Count == 0;
		}

		private static T NullFunc<T>(T t)
		{
			return t;
		}
		public static HashSet<T> ToHashSet<T>(this ICollection<T> self, HashSet<T> hashSet)
		{
			self.ToHashSet(NullFunc, hashSet);
			return hashSet;
		}
		public static HashSet<T1> ToHashSet<T, T1>(this ICollection<T> self, Func<T, T1> func)
		{
			var hash = new HashSet<T1>();
			self.ToHashSet(func, hash);
			return hash;
		}
		public static void ToHashSet<T, T1>(this ICollection<T> self, Func<T, T1> func, HashSet<T1> has)
		{
			foreach (var it in self)
			{
				var value = func(it);
				has.Add(value);
			}

		}
	}
}