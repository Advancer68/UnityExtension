﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace CxExtension
{
	public static class EnumerableExt
	{
		/// <summary>
		/// 匹配到迭代器中满足条件的位置
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		public static bool MatchPos<T>(this IEnumerator<T> self, Predicate<T> func)
		{
			var enume = self;
			if (func == null)
			{
				return false;
			}

			do
			{
				/*if(enume.Current.Equals(default(T)))
				{
					continue;
				}*/
				if (func(enume.Current))
				{
					return true;
				}
			} while (enume.MoveNext());
			return false;
		}
		public static T Min<T>(this IEnumerable<T> list,Func<T,float> func,out float minf)
		{
			T t = default(T);
			minf = float.MaxValue;
			float curf = 0;
			foreach(var item in list)
			{
				curf = func(item);
				if(curf < minf)
				{
					minf = curf;
					t = item;
				}
			}
			return t;
		}
		public static T Min<T>(this IEnumerable<T> list,Func<T,float> func)
		{
			float minf;
			return list.Min(func,out minf);
		}
		public static T Min<T>(this IEnumerable<T> list,Func<T,int> func)
		{
			int min;
			return list.Min(func,out min);
		}
		public static T Min<T>(this IEnumerable<T> list,Func<T,int> func,out int minf)
		{
			T t = default(T);
			minf = int.MaxValue;
			int curf = 0;
			foreach(var item in list)
			{
				curf = func(item);
				if(curf < minf)
				{
					minf = curf;
					t = item;
				}
			}
			return t;
		}
		public static T Max<T>(this IEnumerable<T> list,Func<T,float> func,out float maxf)
		{
			T t = default(T);
			maxf = float.MinValue;
			float curf = 0;
			foreach(var item in list)
			{
				curf = func(item);
				if(curf > maxf)
				{
					maxf = curf;
					t = item;
				}
			}
			return t;
		}
		public static T Max<T>(this IEnumerable<T> list,Func<T,float> func)
		{
			float max;
			return list.Max(func,out max);
		}

		/// <summary>
		///查找一个元素,如果找到返回true,否则返回false
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="func"></param>
		/// <param name="result">找到的元素</param>
		/// <returns></returns>
		public static bool FindA<T>(this IEnumerable<T> self,Predicate<T> func,out T result)
		{
			foreach(var item in self)
			{
				if(func(item))
				{
					result = item;
					return true;
				}
			}
			result = default(T);
			return false ;
		}

		public static bool ContainsF<T>(this IEnumerable<T> self, Predicate<T> func)
		{
			T t;
			return self.FindA(func, out t);
		}
		/// <summary>
		/// 取得一个计时器,并匹配到满足条件的位置
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		public static IEnumerator<T> MatchPos<T>(this IEnumerable<T> self,Predicate<T> func)
		{
			var enume = self.GetEnumerator();
			enume.MatchPos(func);
			return enume;
		}
	}
}
