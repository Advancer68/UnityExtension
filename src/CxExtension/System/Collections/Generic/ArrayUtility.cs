﻿using System.Collections.Generic;
using System;
using System.Text;

namespace CxExtension
{
	public static class ArrayUtility
	{
		public static string ToStringD<T>(this T[] self)
		{
			return self.ToStringD(0, self.Length);
		}
		/// <summary>
		/// combine all string by split
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="start"></param>
		/// <param name="count"></param>
		/// <param name="split"></param>
		/// <param name="lineCount"></param>
		/// <returns></returns>
		public static string ToStringD<T>(this T[] self,int start,int count,string split = " ",int lineCount = 50)
		{
			var build = new StringBuilder();
			var lineNum = 0;
			for (int i = start, length = Mathfe.ClampRing(start +count,start, self.Length); i < length; i++)
			{
				var it = self[i];
				var str = it.ToString();
				build.Append(str);
				build.Append(split);
				lineNum++;
				if (lineNum >= lineCount)
				{
					lineNum = 0;
					build.AppendLine();
				}
			}

			return build.ToString();
		}
		public static T FindA<T>(this T[] self,Func<T,bool> func)
		{
			var t = default(T);
			self.ForEachBreakOnTrue(it =>
			{
				var b = func(it);
				if(b)
				{
					t = it;
				}
				return b;
			});
			return t;
		}

		public static List<T> FindAll<T>(this T[] self, Func<T, bool> func,List<T> list)
		{
			if (list == null)
			{
				list = new List<T>();
			}
			self.ForEach(it =>
			{
				if (func(it))
				{
					list.Add(it);
				}
			});
			return list;
		}
		public static List<T> FindAll<T>(this T[] self,Func<T,bool> func)
		{
			var list = self.FindAll(func, null);
			return list;
		}
		/// <summary>
		/// 添加一个元素懂啊Array中
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="item"></param>
		/// <returns></returns>
		public static T[] Add<T>(this T[] self,T item)
		{
			return (new List<T>(self) { item }).ToArray();
		}
		public static T[] Copy<T>(T[] self)
		{
			return (new List<T>(self)).ToArray();
		}
		/// <summary>
		/// 移动某索引的元素到新的索引
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="oldIndex"></param>
		/// <param name="newIndex"></param>
		/// <returns></returns>
		public static T[] Move<T>(T[] self,int oldIndex,int newIndex)
		{
			var ts = new List<T>(self);
			var item = ts[oldIndex];
			ts.RemoveAt(oldIndex);
			ts.Insert(newIndex,item);
			return ts.ToArray();
		}

		public static T[] Remove<T>(T[] self,T item)
		{
			var ts = new List<T>(self);
			ts.Remove(item);
			return ts.ToArray();
		}

		public static T[] RemoveAt<T>(T[] self,int index)
		{
			var ts = new List<T>(self);
			ts.RemoveAt(index);
			return ts.ToArray();
		}

		public static T[] Insert<T>(T[] self,T item,int index)
		{
			var ts = new List<T>(self);
			ts.Insert(index,item);
			return ts.ToArray();
		}


		public static T[] Sort<T>(T[] self)
		{
			var ts = new List<T>(self);
			ts.Sort();
			return ts.ToArray();
		}

		public static T[] Sort<T>(T[] self,IComparer<T> comparer)
		{
			var ts = new List<T>(self);
			ts.Sort(comparer);
			return ts.ToArray();
		}

		public static T[] Reverse<T>(T[] self)
		{
			var ts = new List<T>(self);
			ts.Reverse();
			return ts.ToArray();
		}
		/// <summary>
		/// 把数组转换成带主键的字典便于查询
		/// </summary>
		/// <typeparam name="K"></typeparam>
		/// <typeparam name="T"></typeparam>
		/// <param name="self"></param>
		/// <param name="func"></param>
		/// <returns></returns>
		public static Dictionary<K,T> ToDictionary<K, T>(this T[] self,Func<T,K> func)
		{
			var dic = new Dictionary<K,T>(self.Length);
			self.ForEach(it =>
			{
				var k = func(it);
				dic[k] = it;
			});
			return dic;
		}
	}
}