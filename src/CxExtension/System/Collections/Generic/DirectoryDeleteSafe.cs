﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;

namespace CxExtension
{
	/// <summary>
	/// 可在遍历中安全删除的字典,遍历需要使用ForeachAll
	/// </summary>
	/// <typeparam name="K"></typeparam>
	/// <typeparam name="V"></typeparam>
	public class DirectoryDeleteSafe<K,V> :CollectionDeleteSafeTemplate<Dictionary<K,V>,KeyValuePair<K,V>>,IDictionary<K,V>
	{
		public override void CopyTo(KeyValuePair<K, V>[] array, int arrayIndex)
		{
			throw new NotImplementedException();
		}
		#region Overrides of CollectionDeleteSafeTemplate<Dictionary<K,V>,KeyValuePair<K,V>>

		protected override void foreach_(Action<KeyValuePair<K, V>> func)
		{
			foreach (var it in mCollection)
			{
				func(it);
			}
		}

		protected override void foreach_(Func<KeyValuePair<K, V>, bool> func, Action<KeyValuePair<K, V>> selectAction)
		{
			foreach (var it in mCollection)
			{
				if (func(it) == false) continue;
				if (selectAction != null) selectAction(it);
			}
		}

		#endregion

		

		#region Implementation of IDictionary<K,V>

		public void Add(K key, V value)
		{
			Add(new KeyValuePair<K, V>(key,value));
		}

		public bool ContainsKey(K key)
		{
			return mCollection.ContainsKey(key);
		}

		public bool Remove(K key)
		{
			return mCollection.Remove(key);
		}

		public bool TryGetValue(K key, out V value)
		{
			return mCollection.TryGetValue(key, out value);
		}

		public V this[K key]
		{
			get { return mCollection[key]; }
			set { mCollection[key] = value; }
		}

		public ICollection<K> Keys
		{
			get { return mCollection.Keys; }
		}

		public ICollection<V> Values
		{
			get { return mCollection.Values; }
		}

		#endregion
	}
}