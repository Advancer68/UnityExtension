﻿using System;
using System.Text;

namespace CxExtension
{
	public static class StringExt
	{
		public static string SubStringEndWith(this string self, string value)
		{
			var indexOf = self.LastIndexOf(value, StringComparison.Ordinal);
			if (indexOf > 0)
			{
				indexOf += value.Length;
				return self.Substring(indexOf, self.Length - indexOf);
			}
			return string.Empty;
		}
		public static string SubStringEndWith(this string self, char value)
		{
			var indexOf = self.LastIndexOf(value);
			if (indexOf > 0)
			{
				indexOf += 1;
				return self.Substring(indexOf, self.Length - indexOf);
			}
			return string.Empty;
		}
		public static string SubStringStartWith(this string self, string value)
		{
			var indexOf = self.IndexOf(value, StringComparison.Ordinal);
			if (indexOf > 0)
			{
				//indexOf++;
				indexOf += 1;
				return self.Substring(indexOf, self.Length - indexOf);
			}

			return string.Empty;
		}
		public static string SubStringStartWith(this string self, char value)
		{
			var indexOf = self.IndexOf(value);
			if (indexOf > 0)
			{
				indexOf += 1;
				return self.Substring(indexOf, self.Length - indexOf);
			}

			return string.Empty;
		}
		/// <summary>
		/// 优化版本的string比较
		/// </summary>
		/// <param name="self"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool EqualsOrdinal(this string self, string value)
		{
			return StringI.EqualsOrdinal(self, value);
		}

		/// <summary>
		/// 移除小数字符串最后多余的零
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static string RemovePointLastZero(this string self)
		{
			var index = self.LastIndexOf('.');
			var len = self.Len();
			var lastBt = len;
			for (int i = len - 1; i >= 0; i--)
			{
				var c = self[i];
				if (c != '0' || c == '.')
				{
					lastBt = i + 1;
					if (c == '.')
					{
						lastBt--;
					}
					break;
				}
			}

			var str = self.Substring(0, lastBt);
			return str;
		}
		public static int Len(this string self)
		{
			return self.IsNullOrEmpty() ? 0 : self.Length;
		}


		public static string[] Split(this string self, int step)
		{
			var count = self.Len();
			step = Math.Abs(step);
			var lv = count / step;
			var list = new string[lv];

			return list;
		}
		/// <summary>
		/// 首字符大写
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static string ToFirstUpper(this string self)
		{
			if (self.IsNullOrEmpty())
			{
				return self;
			}

			return self.Substring(0, 1).ToUpper() + self.Substring(1);
		}

		public static long LetterToIndex(this string self)
		{
			var v = LetterToNumber(self);
			if (v > 0)
			{
				v--;
			}

			return v;
		}

		public static long LetterToNumber(this string self)
		{
			if (string.IsNullOrEmpty(self))
			{
				return 0;
			}
			var lstr = self.Trim().ToLower();
			var b = 0;
			long num = 0;
			for (var i = lstr.Length - 1; i >= 0; i--)
			{
				int n = lstr[i];
				n -= 96;
				num += (long)Math.Pow(26, b) * n;
				//Console.WriteLine(i);
				b++;
			}
			//Console.WriteLine(num);
			//Console.ReadKey();

			return num;
		}
		/// <summary>
		/// 移除字符 拓展版,pos支持负数
		/// </summary>
		/// <param name="self"></param>
		/// <param name="startPos">如果为负数,则startpos = length - startpos</param>
		/// <param name="count"></param>
		/// <returns></returns>
		public static string RemoveE(this string self, int startPos, int count = -1)
		{
			if (startPos < 0)
			{
				startPos = self.Length + startPos;
			}

			if (count == -1)
			{
				count = self.Length - startPos;
			}
			return self.Remove(startPos, count);
		}

	}
}