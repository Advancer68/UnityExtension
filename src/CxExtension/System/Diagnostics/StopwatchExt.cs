﻿using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace CxExtension.System.Diagnostics
{
	public static class StopwatchExt
	{

		public static float ElapsedSeconds(this Stopwatch self)
		{
			return self.ElapsedMilliseconds / 1000f;
		}

		public static void StopAndLog(this Stopwatch self,string text,long minLogMs = 0)
		{
			self.Stop();
			if (self.ElapsedMilliseconds > minLogMs)
			{
#if true
				
#endif
				Debug.LogFormat("{0} cost:{1}ms", text,self.ElapsedMilliseconds);
				
			}
		}
	}
}