﻿using System.Runtime.CompilerServices;
namespace System.Reflection
{
	public static class MethodInfoExt
	{
		/// <summary>
		/// 检查函数是否是拓展函数
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static bool IsExtension(this MethodInfo self)
		{
			return self.IsDefined(typeof(ExtensionAttribute), true);
		}
	}
}
