﻿using System;
using System.IO;

namespace CxExtension
{
	public static class StreamWriterExt
	{
		/// <summary>
		/// 删除末尾的新行符号
		/// </summary>
		/// <param name="self"></param>
		public static void DeleteLastNewLine(this StreamWriter self)
		{
			self.DeleteLast(self.NewLine);
		}
		/// <summary>
		/// 删除末尾text bytes长度的内容
		/// </summary>
		/// <param name="self"></param>
		/// <param name="text"></param>
		public static void DeleteLast(this StreamWriter self,string text)
		{
			if (self.AutoFlush == false)
			{
				self.Flush();
			}
			var stream = self.BaseStream;
			var bytes = self.Encoding.GetBytes(text);
			var newLength = stream.Length  - bytes.Length;
			stream.SetLength(newLength);
		}

	}
}