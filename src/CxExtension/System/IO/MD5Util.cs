﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using CxExtension.System;

namespace CxExtension
{
	public static class MD5Util
	{
		private static readonly MD5 hasher;                      //

		static MD5Util()
		{
			hasher = MD5.Create();
		}
		public static string GetHashStr(string str)
		{
			var bytes = Encoding.UTF8.GetBytes(str);
			return GetHashStr(bytes);
		}
		public static string GetHashStr(Stream stream)
		{
			var md5 = hasher.ComputeHash(stream);
			var str = BitConverterE.ToHashStr(md5);
			return str;
		}
		public static string GetHashStr(byte[] bytes)
		{
			var md5 = hasher.ComputeHash(bytes);
			var str = BitConverterE.ToHashStr(md5);
			return str;
		}

	}
}