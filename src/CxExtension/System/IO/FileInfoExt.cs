﻿using System.IO;

namespace CxExtension
{
	public static class FileInfoExt
	{

		public static string GetHashStr(this FileInfo self)
		{
			var str = string.Empty;
			using (var stream = self.OpenRead())
			{
				str = MD5Util.GetHashStr(stream);
			}

			return str;
		}

	}
}