﻿namespace CxExtension
{
	public static class ObjectBaseExt
	{
		/// <summary>
		/// if self or default value any one has content then return true,otherwise return false
		/// </summary>
		/// <param name="self"></param>
		/// <param name="defaultValue"></param>
		/// <param name="resultValue"></param>
		/// <returns></returns>
		public static bool HasValueOrDefault<T>(this T self, T defaultValue, out T resultValue) where T :class
		{
			var result = self.IsNull() ? defaultValue : self;
			resultValue = result;
			return result.IsNoNull();
		}
		/// <summary>
		/// if self is null then return true
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static bool IsNull<T>(this T self) where T :class
		{
			var value = self == null;
			return value;
		}
		/// <summary>
		/// if self is not null then return true
		/// </summary>
		/// <param name="self"></param>
		/// <returns></returns>
		public static bool IsNoNull<T>(this T self) where T :class
		{
			return self != null;
		}
	}
}