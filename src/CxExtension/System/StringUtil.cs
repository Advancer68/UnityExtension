﻿using System.Collections.Generic;
using System.Text;
using CxExtension;
using JetBrains.Annotations;

namespace System
{
	public static class StringUtil
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="self"></param>
		/// <param name="split"></param>
		/// <returns></returns>
		public static string Combine(this ICollection<string> self, string split)
		{
			return self.Combine(split, string.Empty, string.Empty);
		}
		public static string CombineWithStart(this ICollection<string> self, string split, string start)
		{
			return self.Combine(split, start, string.Empty);
		}
		public static string CombineWithEnd(this ICollection<string> self, string split, string end)
		{
			return self.Combine(split, string.Empty, end);
		}

		public static string Combine(this ICollection<string> self, string split,
			string start, string end)
		{
			return self.Combine(it => it, split, start, end);
		}
		public static string Combine<T>(this ICollection<T> self, [NotNull] Func<T, string> toStrFunc, string split, string start = "", string end = "")
		{
			if (self == null) throw new ArgumentNullException("self");
			if (toStrFunc == null) throw new ArgumentNullException("toStrFunc");

			var bld = new StringBuilder();
			bld.Append(start);
			var len = self.Count;
			if (len > 0)
			{
				foreach (var it in self)
				{
					var str = toStrFunc(it);
					bld.Append(str);
					bld.Append(split);
				}
				bld.RemoveLast(split.Length);
			}
			bld.Append(end);
			return bld.ToString();
		}


		public static string RemovePointLastZero(string self)
		{
			return self.RemovePointLastZero();
		}

	}
}