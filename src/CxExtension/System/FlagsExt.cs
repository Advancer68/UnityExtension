﻿namespace CxExtension
{
	public static class FlagsExt
	{
		public static bool HasFlag(this int self, int value)
		{
			var result = self & value;
			return result == value;
		}

		public static int RemoveFlag(this int self, int value)
		{
			var result = self ^ value;
			return result;
		}
	}
}