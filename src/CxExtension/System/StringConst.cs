﻿namespace CxExtension
{
	/// <summary>
	/// 字符串常量
	/// </summary>
	public static class StringConst
	{
		public const string NewLineWindows = "\n\r";
		public const string NewLineUnix = "\n";
		public const string NewLineMac = "\r";
		public const string Tab = "\t";
	}
}