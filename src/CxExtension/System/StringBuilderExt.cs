﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class StringBuilderExt
{
	public static void ReplaceFirst(this StringBuilder self,string old, string news)
	{
		var str = self.ToString();
		var pos = str.IndexOf(old, StringComparison.Ordinal);
		if (pos == -1)
		{
			return;
		}
		var end = Mathf.Min(pos + old.Length,self.Length);
		var count = end - pos;
		self.Replace(old, news, pos, count);
	}
	public static void Clear(this StringBuilder self)
	{
		self.Length = 0;
	}
	/// <summary>
	/// remove last target num char
	/// </summary>
	/// <param name="self"></param>
	/// <param name="count">char count</param>
	/// <returns></returns>
	public static StringBuilder RemoveLast(this StringBuilder self,int count)
	{
		if (count < 0) throw new ArgumentOutOfRangeException("num");
		return self.RemoveE(-count);
	}
	public static StringBuilder RemoveE(this StringBuilder self,int startPos, int count = -1)
	{
		if (startPos < 0)
		{
			startPos = self.Length + startPos;
		}

		if (count == -1)
		{
			count = self.Length - startPos;
		}
		return self.Remove(startPos, count);
	}

	public static StringBuilder AppendLineFormat(this StringBuilder self, string format, params object[] args)
	{
		return self.AppendFormat(format, args).AppendLine();

	}
	public static StringBuilder AppendFormatLine(this StringBuilder self,string format,params object[] args)
	{
		return self.AppendLineFormat(format, args);
	}

}
