﻿using System;

namespace CxExtension
{
	public static class AppDomainCache
	{
		private static CacheDictionary<string,AssemblyCache> mCache; //

		static AppDomainCache()
		{
			mCache = new CacheDictionary<string, AssemblyCache>();
			var list = AppDomain.CurrentDomain.GetAssemblies();
			foreach (var assembly in list)
			{
				var cache = new AssemblyCache(assembly);
				mCache.Set(assembly.FullName,cache);
			}
		}

		public static Type GetType(string fullName)
		{
			Type type = null;
			foreach (var pair in mCache.CacheMap)
			{
				type = pair.Value.Get(fullName);
				if (type != null)
				{
					break;
				}
			}
			return type;
		}
	}
}