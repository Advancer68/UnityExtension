﻿/*************************************************
  Copyright (C), 2014-2017, lmd Tech. Co., Ltd.
  文 件  名:	Assets\Scripts\Tools\CxExtension\TimeUtil.cs
  作       者:	陈佳
  版       本:	1.0        
  完成日期:	2017/07/13       
  功能描述:	时间工具类,提供时间
  主要功能: 获取当前时间总的毫秒数
*************************************************/
using CxExtension;
using System;
using UnityEngine;

namespace UnityExtensions
{
	public static class TimeUtil
	{
		private static readonly long mBaseTicks = 621355968000000000;
		private static readonly long mTicks2Second = 10000000;
		private static readonly long mTicks2Ms = 10000;

		static TimeUtil()
		{
		}

		public static string Second2String(long s)
		{
			var time = TimeSpan.FromSeconds(s);
			var str = $"{time:T}";
			if (!str.IsNullOrEmpty())
			{
				str = str.Replace(".", "D ");
			}

			return str;
		}

		public static string Second2String(string s)
		{
			long.TryParse(s,out var num);
			return Second2String(num);
		}

		public static int ToTimestamp(this DateTime self)
		{
			return (int) ((self.Ticks - mBaseTicks) / mTicks2Second);
		}
		public static long ToTimestampMs(this DateTime self)
		{
			return ((self.Ticks - mBaseTicks) / mTicks2Ms);
		}
		/// <summary>
		/// 取得当前时间总的毫秒数
		/// 时间是相对于第一次调用此函数的时间,也就是说第一次调用很有可能取得的值是0
		/// </summary>
		public static long nowTotalMilliseconds => timestampMs;

		/// <summary>
		/// 秒级时间戳
		/// </summary>
		public static int timestamp => DateTime.UtcNow.ToTimestamp();
		/// <summary>
		/// 毫秒级时间戳
		/// </summary>
		public static long timestampMs => DateTime.UtcNow.ToTimestampMs();

		/// <summary>
		/// 取得当前时间总的毫秒数
		/// 时间是相对于第一次调用此函数的时间,也就是说第一次调用很有可能取得的值是0
		/// </summary>
		public static int nowTotalSeconds => timestamp;
		/// <summary>
		///取得一个时间,从游戏开始时计时,的时间毫秒数
		/// </summary>
		public static long realtimeSinceStartupMs
		{
			get
			{
				var v = ((double)Time.realtimeSinceStartup) * 1000;
				return (long)v;
			}
		}
	}
}