﻿using System.IO;
using System.Text;

public static class PathUtil
{
	public static readonly string WindowSplit = "\\";
	public static readonly char WindowSplit1 = '\\';
	public static readonly string UnixSplit = "/";
	public static readonly char UnixSplitC = '/';
	private static readonly StringBuilder sBuilder = new StringBuilder();                      //

	public static string ChangePostfix(string path, string postfix)
	{
		if (postfix.StartsWith("."))
		{
			postfix = postfix.Remove(0, 1);
		}
		var dir = Path.GetDirectoryName(path);
		var fileName = Path.GetFileNameWithoutExtension(path) + postfix;
		var newPath = PathUtil.Combine(dir,fileName);
		return newPath;
	}
	/// <summary>
	/// 更换跟目录,常用于吧某个文件从一个目录移动到另一个目录
	/// </summary>
	/// <param name="path"></param>
	/// <param name="srcRootDir"></param>
	/// <param name="destRootDir"></param>
	/// <returns></returns>
	public static string ChangeRootPath(string path, string srcRootDir,string destRootDir)
	{
		var relative = RelativePath(path,srcRootDir);
		var newPath = Path.Combine(destRootDir,relative);
		newPath = ToUnixPath(newPath);
		return newPath;
	}
	/// <summary>
	/// 取得相对于rootDir的相对路径
	/// </summary>
	/// <param name="path"></param>
	/// <param name="rootDir"></param>
	/// <returns></returns>
	public static string RelativePath(string path,string rootDir)
	{
		path = ToUnixPath(path);
		rootDir = ToUnixPath(rootDir);
		var has = path.StartsWith(rootDir,true,null);
		var hasU= rootDir.EndsWith(UnixSplit);
		if (hasU == false)
		{
			rootDir = rootDir + UnixSplit;
		}
		var relative = string.Empty;
		if (has)
		{
			relative = path.Substring(rootDir.Length);
		}

		return relative;
	}
	/// <summary>
	/// Combine all path then return Unix path
	/// </summary>
	/// <param name="args"></param>
	/// <returns></returns>
	public static string Combine(params string[] args)
	{
		sBuilder.Clear();
		foreach (var arg in args)
		{
			var argstr = arg;
			argstr = ToUnixPath(argstr);
			var p1 = argstr.EndsWith(UnixSplit) ? "" : UnixSplit;
			sBuilder.AppendFormat("{0}{1}", arg, p1);
		}
		sBuilder.Length -= 1;
		//sBuilder.Replace(WindowSplit, UnixSplit);
		return sBuilder.ToString();
	}

	/// <summary>
	/// 获取远程格式的路径（带有file:// 或 http:// 前缀）。
	/// </summary>
	/// <param name="path">原始路径。</param>
	/// <returns>远程格式路径。</returns>
	public static string GetRemotePath(params string[] args)
	{
		var newPath = Combine(args);
		newPath = ToUnityRemotePath(newPath);
		return newPath;
	}

	public static string ToUnityRemotePath(string path)
	{
		var newPath = path.Contains("://") ? path : ("file:///" + path).Replace("file:////", "file:///");
		return newPath;
	}
	public static string ToUnixPath(string path)
	{
		var str = path.Replace(WindowSplit, UnixSplit);
		str = str.Replace(WindowSplit1, UnixSplitC);
		return str;
	}
	public static string ToUnixPath1(string path)
	{
		return path.Replace(WindowSplit1, UnixSplitC);
	}
	public static string ToRightPath(string path)
	{
		return ToUnixPath(path);
	}
	public static string ToWindowsPath(string path)
	{
		return path.Replace(UnixSplit, WindowSplit);
	}

	public static string ToLeftPath(string path)
	{
		return ToWindowsPath(path);
	}
	/// <summary>
	/// in:aa/bb/cc/ee.text
	/// return:text
	/// in:aa/bb/cc/ee.text.xx
	/// return:xx
	/// </summary>
	/// <param name="path"></param>
	/// <returns></returns>
	public static string GetExtensionName(this string path)
	{
		if (path == null)
			return (string)null;

		int length = path.Length;
		int num = length;
		while (--num >= 0)
		{
			char ch = path[num];
			if (ch == '.')
			{
				num++;
				return num != length - 1 ? path.Substring(num, length - num) : string.Empty;
			}
			if ((int)ch == (int)Path.DirectorySeparatorChar || (int)ch == (int)Path.AltDirectorySeparatorChar || (int)ch == (int)Path.VolumeSeparatorChar)
				break;
		}
		return string.Empty;
	}
	/// <summary>
	/// get file path with out ext name
	/// in:aa/bb/cc/ee.text
	/// return:aa/bb/cc/ee
	/// in:aa/bb/cc/ee.text.xx
	/// return:aa/bb/cc/ee.text
	/// </summary>
	/// <param name="path"></param>
	/// <returns></returns>
	public static string GetPathWithoutExtensionName(this string path)
	{
		if (path == null)
			return (string)null;

		int length = path.Length;
		int num = length;
		while (--num >= 0)
		{
			char ch = path[num];
			if (ch == '.')
			{
				//num--;
				return num != 0 ? path.Substring(0, num) : string.Empty;
			}
			if ((int)ch == (int)Path.DirectorySeparatorChar || (int)ch == (int)Path.AltDirectorySeparatorChar || (int)ch == (int)Path.VolumeSeparatorChar)
				break;
		}
		return string.Empty;
	}
}
