﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#pragma warning disable 649

namespace Config
{
    public class LanguageConfig : BaseArchive<LanguageConfig>
    {
        [Serializable]
        private class KeyValue
        {
            public string id = "";
            public string value = "";
        }
        //[SerializeField]
        //private string class_name = "";
        [SerializeField]
        private List<KeyValue> dataMaps;
        public string GetTextById(string id)
        {
            if(dataMaps == null)
            {
                Debug.LogWarningFormat("没有找到id ={0} 的语言配置 ", id);
                return "null";
            }
            var v = dataMaps.Find((it) => it.id == id);
            if (v != null)
            {
                return v.value;
            }
            else
            {
                Debug.LogWarningFormat("没有找到id ={0} 的语言配置 ", id);
                return "null";
            }
        }
    }

}
