﻿using UnityEngine;
using System.Collections;
using Config;
namespace Configs
{
	public class TestConfig : BaseConfig<TestConfig>
	{
		public string navId { get; set; }
		public string fishType { get; set; }
		public string value3 { get; set; }
		public float angle { get; set; }
		public Vector3 pos { get; set; }
	}
}
