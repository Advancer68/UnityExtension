﻿using System;
using System.Collections.Generic;
using System.IO;
using CxExtension.System.Diagnostics;

namespace Config.CLV
{
	/// <summary>
	/// Clv Wrap external
	/// </summary>
	public partial class Clv
	{
		public static List<T> LoadTableList<T>(string file) where T : new()
		{
			//ProfilerF.BeginSample("Clv.LoadTableList");
			var stream = File.OpenText(file);
			var list = new List<T>();
			ParseToListWithField<List<T>, T>(stream, list, AddToList);
			//ProfilerF.EndSample();
			return list;
		}
		private static void AddToList<T>(List<T> list, T value)
		{
			list.Add(value);
		}
		public static List<T> ParseToListWithField<T>(byte[] bytes) where T : new()
		{
			var list = new List<T>();
			var memoryStream = new MemoryStream(bytes);
			return ParseToListWithField<List<T>, T>(memoryStream, list, AddToList<T>);
		}
		public static void ParseToCollectionWithField<C,T>(byte[] bytes,C collection,Action<C,T> addListAction) where T : new()
		{
			//var list = new List<T>();
			var memoryStream = new MemoryStream(bytes);
			ParseToListWithField(memoryStream, collection, addListAction);
		}
		public static C ParseToListWithField<C, T>(Stream stream, C list, Action<C, T> addListAction) where T : new()
		{
			var streamReader = new StreamReader(stream);

			return ParseToListWithField(streamReader, list, addListAction);
		}
	}

}