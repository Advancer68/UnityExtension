﻿public interface ICondition
{
	bool Check();
}