﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FlagsAnd.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the FlagsAnd type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

/// <inheritdoc />
/// <summary>
/// 与运算的标记类
/// </summary>
public class FlagsAnd : Flags
{
	public override bool Check()
	{
		for(var i = 0; i < mFlags.Count; i++)
		{
			var it = mFlags[i];
			if(it.Check() == false)
			{
				return false;
			}
		}
		return true;
	}

	public FlagsAnd(bool value)
		: base(value)
	{
	}
}