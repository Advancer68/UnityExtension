﻿using UnityEngine;

public class CInterval : ICondition
{
	private float mInterval;                      //
	private float mNextTime;                      //

	public CInterval()
	{
		
	}
	public CInterval(float interval)
	{
		mInterval = interval;
	}
	public bool Check()
	{
		return Time.realtimeSinceStartup >= mNextTime;
	}


	public void Reset()
	{
		mNextTime = Time.realtimeSinceStartup + mInterval;
	}

	public void SetInterval(float v)
	{
		mInterval = v;
	}

}