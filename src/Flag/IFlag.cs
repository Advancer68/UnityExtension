﻿
using System;

public interface IFlag : ICondition
{
	void True();
	void False();

	void Set(bool value);
}

public interface IReset
{
	void Reset();
}

public interface IRecycle<T>
{
	void Recycle(T item);

}

public interface IPoolBase
{

}
public interface IPool<T>: IPoolBase,IRecycle<T>
{
	T Get();
}

public interface IPoolAsync<T>:IRecycle<T>
{
	void Get(Action<T> call);
}
