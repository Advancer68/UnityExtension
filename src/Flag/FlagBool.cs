﻿


using System;

using UnityEngine;

public class FlagBool : Flag
{
	private bool mFlag = false;

	public FlagBool() { }
	public FlagBool(bool value)
	{
		mFlag = value;
	}

	public override void Set(bool value)
	{
		mFlag = value;
	}

	public override bool Check()
	{
		return mFlag;
	}

	public override void True()
	{
		mFlag = true;
	}

	public override void False()
	{
		mFlag = false;
	}
}

public class FlagBoolObj : FlagBool
{
	private object mObj = null;

	public object Obj { get { return mObj; } }


	public FlagBoolObj(bool value, object obj)
		: base(value)
	{
		if (obj == null)
		{
			Debug.LogErrorFormat("初始化FlagBoolObj是obj不能为null");
			return;
		}
		mObj = obj;
	}
}
