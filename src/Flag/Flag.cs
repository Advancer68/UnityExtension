﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Flag.cs" company="fy">
//   
// </copyright>
// <summary>
//   Defines the Flag type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

public abstract class Flag : IFlag
{
	private Flags mParent;

	public abstract void True();

	public abstract void False();

	public abstract void Set(bool value);

	public abstract bool Check();

	public void Release()
	{
		mParent.Remove(this);
		mParent = null;
	}

	public void SetParent(Flags parent)
	{
		mParent = parent;
	}

	public static implicit operator bool(Flag value)
	{
		return value != null && value.Check();
	}
}