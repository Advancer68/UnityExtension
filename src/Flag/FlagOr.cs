﻿
/// <summary>
/// 或运算Flag
/// </summary>
public class FlagOr : Flags
{
	public override bool Check()
	{
		for(var i = 0; i < mFlags.Count; i++)
		{
			var it = mFlags[i];
			if(it.Check())
			{
				return true;
			}
		}
		return false;
	}

	public FlagOr(bool value)
		: base(value)
	{
	}
}