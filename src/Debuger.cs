﻿/*************************************************
  Copyright (C), 2014-2017, lmd Tech. Co., Ltd.
  文 件  名:	
  作       者:	陈佳
  版       本:	1.0        
  完成日期:	2017//      
  功能描述:	
  主要功能: 
*************************************************/
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CxExtension
{
	public static class Debuger
	{
		public static void ErrFormat(string format, params object[] args)
		{
			Debug.LogErrorFormat(format, args);
		}

		internal static void WrnFormat(string format, params object[] args)
		{
			Debug.LogWarningFormat(format, args);
		}
	}
}
