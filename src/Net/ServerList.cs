﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ServerList{

	[Serializable]
	public struct SHostInfo
	{
		public string name;
		public string host;
		public uint port;
	}

	public List<SHostInfo> Servers;
	//[HideInInspector]
	public int SelectIdx;

	public SHostInfo GetSelect()
	{
		return Servers[SelectIdx];
	}
}
