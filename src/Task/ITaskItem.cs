﻿namespace CxExtension.Task
{
	public interface ITaskItem:IReset
	{
		bool Start();
		bool Update(float dt);
		void End();
	}

	public static class ITaskItemExt
	{

		public static void Restart(this ITaskItem self)
		{
			self.End();
			self.Reset();
			self.Start();
		}

	}
}