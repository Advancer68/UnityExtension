﻿

namespace CxExtension
{
	public interface ITaskAgent<T>
	{
		T Task { get;}
		void Init();
		
		void Start(T task);
		
		bool Update(float dt);
		
		void End();
		
		void Reset();
	}

	public abstract class TaskAgent<T>:ITaskAgent<T>
	{
		public T Task { get; private set; }

		public virtual void Init()
		{

		}

		public virtual void Start(T task)
		{
			Task = task;
		}

		public abstract bool Update(float dt);

		public virtual void End() { }

		public virtual void Reset()
		{
			Task = default(T);
		}
	}
}