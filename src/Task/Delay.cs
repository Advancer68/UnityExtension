﻿using System;
using JetBrains.Annotations;

namespace CxExtension.Task
{
	public class DelayCallA1 : DelayCall
	{
		private Action mCall; //

		#region Overrides of DelayCall

		protected override void CallAction()
		{
			base.CallAction();
			mUsedTime = 0;
			if (mCall != null) mCall();
		}

		#endregion

		public DelayCallA1(float dt, Action call) : base(dt)
		{
			//if (call == null) throw new ArgumentNullException(nameof(call));
			mCall = call;
		}

		public void SetDelayAction(Action v)
		{
			mCall = v;
		}

	}

	public class WhileCount : ITaskItem
	{
		protected ITaskItem mTask;//
		protected int mCountCur = 0;//次数
		protected int mCountMax = 1;//次数
		public event Action OnCompleteE;

		public WhileCount(ITaskItem task,int excuteCount)
		{
			mTask = task;
			mCountMax = excuteCount;
		}

		#region Implementation of IReset

		public void Reset()
		{
			mCountCur = 0;
		}

		public bool Start()
		{
			mTask.Start();
			return true;
		}

		public bool Update(float dt)
		{
			var result = mTask.Update(dt);
			if (result)
			{
				mCountCur++;
				mTask.Restart();
				if (mCountCur >= mCountMax)
				{
					return true;
				}
			}

			return false;
		}

		public void End()
		{
			if (OnCompleteE != null) OnCompleteE();
		}

		#endregion
	}
	public class DelayCall:Delay
	{

		public DelayCall(float dt) : base(dt)
		{

		}

		#region Overrides of Delay

		protected virtual void CallAction() { }
		public override bool Update(float dt)
		{
			var result = base.Update(dt);
			if (result)
			{
				CallAction();
			}
			return true;
		}

		#endregion
	}

	public abstract class TaskItemBase : ITaskItem
	{
		#region Implementation of IReset

		public virtual void Reset()
		{
		}

		#endregion

		#region Implementation of ITaskItem

		public virtual bool Start()
		{
			return true;
		}

		public abstract bool Update(float dt);

		public virtual void End()
		{
		}

		#endregion
	}
	public class Delay : TaskItemBase
	{
		protected float mDuration;                      //持续时间
		protected float mUsedTime;                      //使用时间

		public Delay(float dt)
		{
			mDuration = dt;
		}


		public void SetDuration(float v)
		{
			mDuration = v;
		}

		public override bool Update(float dt)
		{
			mUsedTime += dt;
			return mUsedTime >= mDuration;
		}

		public override void Reset()
		{
			base.Reset();
			mUsedTime = 0;
		}
	}
}