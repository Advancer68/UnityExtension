﻿using System;
using UnityEngine;
using UnityExtensions;

namespace src
{
	public static class AppMain
	{
		private static Transform mMainTransform;                      //
		private static MonoBehaviour mCoroutiner;                      //
		public static event Action UpdateAction;                      //

		public static Transform MainTransform { get { return mMainTransform; } }
		public static MonoBehaviour Coroutiner { get { return mCoroutiner; } }


		public static void SetCoroutiner(MonoBehaviour v)
		{
			mCoroutiner = v;
		}

		public static ActionAsync ActionAsync
		{
			get { return SingleInstance<ActionAsync>.GetInstance(null, it => { UpdateAction += it.Update; }); }
		}
		public static void Init()
		{
			mMainTransform = GameObjectUtility.FindOrNew("Main").transform;
			ActionAsync.AddAction(null);
			//UpdateAction += UpdateSelf;
		}

		public static void Update()
		{
			if (UpdateAction != null) UpdateAction();
		}
	}
}
