﻿using UnityEngine;

namespace src
{
	public class AppMainMono : MonoBehaviour
	{
		private void Awake()
		{
			AppMain.Init();
			AppMain.SetCoroutiner(this);
		}

		private void Update()
		{
			AppMain.Update();
		}
	}
}