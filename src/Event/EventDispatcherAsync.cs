﻿using System.Collections.Generic;
using CxExtension;

public class EventDispatcherAsync : SingletonMonoAsync<EventDispatcherAsync>
{
    List<IEventTrigger> queue = new List<IEventTrigger>();
    private object locked = new object();
    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }
    public void Add(IEventTrigger value)
    {
        lock (locked)
        {
            queue.Add(value);
            //enabled = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (queue.Count > 0)
        {
            lock (locked)
            {
                queue.ForEach(item =>
                {
                    item.Trigger();
                });
                queue.Clear();
                //enabled = false;
            }
        }
    }
}
