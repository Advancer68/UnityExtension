﻿using System;

namespace CxExtension
{
	/// <summary>
	/// 事件触发接口
	/// </summary>
	public interface IEventTrigger
	{
		void Trigger(bool isLog = true);
	}

	/// <summary>
	/// int类型值的修改事件参数传入
	/// </summary>
	public struct SIntChange
	{
		public int prev;//以前值

		public int offsetv;//改变值

		public int curv;//改变后的值
	}

	public class EventBase<T> : IEventTrigger
		where T : EventBase<T>, new()
	{
		private static T selfs;
		protected EventBase() { }
		/// <summary>
		/// 直接调用添加监听者
		/// </summary>
		/// <param name="action"></param>
		public static void AddEventListener(Action<T> action)
		{
			EventDispatcher.AddEventListener(EventName, action);
		}
		public static string EventName
		{
			get { return typeof(T).FullName; }
		}
		/// <summary>
		/// 直接调用移除监听者
		/// </summary>
		/// <param name="action"></param>
		public static void RemoveEventListener(Action<T> action)
		{
			EventDispatcher.RemoveEventListener(EventName, action);
		}
		public static void MarkAsPermanent()
		{
			EventDispatcher.MarkAsPermanent(EventName);
		}

		/// <summary>
		/// 广播事件
		/// </summary>
		/// <param name="args"></param>
		public static void Trigger(T args, bool isLog = true)
		{
			EventDispatcher.TriggerEvent(EventName, args, isLog);
		}
		/// <summary>
		/// 同步触发
		/// </summary>
		/// <param name="isLog"></param>
		public void Trigger(bool isLog = false)
		{
			EventDispatcher.TriggerEvent(EventName, this as T, false);
		}
		/// <summary>
		/// 异步触发
		/// </summary>
		public void TriggerAsync()
		{
			EventDispatcherAsync.instance.Add(this);
		}
		/// <summary>
		/// 获取默认的实例 并覆盖里面的属性 如果没有填写 则默认上次使用的值
		/// </summary>
		public static T defaultInstance
		{
			get
			{
				if(selfs == null)
				{
					selfs = new T();
				}
				return selfs;
			}
		}
	}
}